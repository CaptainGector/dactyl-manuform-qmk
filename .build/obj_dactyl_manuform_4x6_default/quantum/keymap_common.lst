   1               		.file	"keymap_common.c"
   2               	__SP_H__ = 0x3e
   3               	__SP_L__ = 0x3d
   4               	__SREG__ = 0x3f
   5               	__tmp_reg__ = 0
   6               	__zero_reg__ = 1
   7               		.text
   8               	.Ltext0:
   9               		.cfi_sections	.debug_frame
  10               		.section	.text.action_get_macro,"ax",@progbits
  11               		.weak	action_get_macro
  13               	action_get_macro:
  14               	.LFB21:
  15               		.file 1 "quantum/keymap_common.c"
   1:quantum/keymap_common.c **** /*
   2:quantum/keymap_common.c **** Copyright 2012-2017 Jun Wako <wakojun@gmail.com>
   3:quantum/keymap_common.c **** 
   4:quantum/keymap_common.c **** This program is free software: you can redistribute it and/or modify
   5:quantum/keymap_common.c **** it under the terms of the GNU General Public License as published by
   6:quantum/keymap_common.c **** the Free Software Foundation, either version 2 of the License, or
   7:quantum/keymap_common.c **** (at your option) any later version.
   8:quantum/keymap_common.c **** 
   9:quantum/keymap_common.c **** This program is distributed in the hope that it will be useful,
  10:quantum/keymap_common.c **** but WITHOUT ANY WARRANTY; without even the implied warranty of
  11:quantum/keymap_common.c **** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  12:quantum/keymap_common.c **** GNU General Public License for more details.
  13:quantum/keymap_common.c **** 
  14:quantum/keymap_common.c **** You should have received a copy of the GNU General Public License
  15:quantum/keymap_common.c **** along with this program.  If not, see <http://www.gnu.org/licenses/>.
  16:quantum/keymap_common.c **** */
  17:quantum/keymap_common.c **** 
  18:quantum/keymap_common.c **** #include "keymap.h"
  19:quantum/keymap_common.c **** #include "report.h"
  20:quantum/keymap_common.c **** #include "keycode.h"
  21:quantum/keymap_common.c **** #include "action_layer.h"
  22:quantum/keymap_common.c **** #if defined(__AVR__)
  23:quantum/keymap_common.c **** #    include <util/delay.h>
  24:quantum/keymap_common.c **** #    include <stdio.h>
  25:quantum/keymap_common.c **** #endif
  26:quantum/keymap_common.c **** #include "action.h"
  27:quantum/keymap_common.c **** #include "action_macro.h"
  28:quantum/keymap_common.c **** #include "debug.h"
  29:quantum/keymap_common.c **** #include "quantum.h"
  30:quantum/keymap_common.c **** 
  31:quantum/keymap_common.c **** #ifdef BACKLIGHT_ENABLE
  32:quantum/keymap_common.c **** #    include "backlight.h"
  33:quantum/keymap_common.c **** #endif
  34:quantum/keymap_common.c **** 
  35:quantum/keymap_common.c **** #ifdef MIDI_ENABLE
  36:quantum/keymap_common.c **** #    include "process_midi.h"
  37:quantum/keymap_common.c **** #endif
  38:quantum/keymap_common.c **** 
  39:quantum/keymap_common.c **** extern keymap_config_t keymap_config;
  40:quantum/keymap_common.c **** 
  41:quantum/keymap_common.c **** #include <inttypes.h>
  42:quantum/keymap_common.c **** 
  43:quantum/keymap_common.c **** /* converts key to action */
  44:quantum/keymap_common.c **** action_t action_for_key(uint8_t layer, keypos_t key) {
  45:quantum/keymap_common.c ****     // 16bit keycodes - important
  46:quantum/keymap_common.c ****     uint16_t keycode = keymap_key_to_keycode(layer, key);
  47:quantum/keymap_common.c **** 
  48:quantum/keymap_common.c ****     // keycode remapping
  49:quantum/keymap_common.c ****     keycode = keycode_config(keycode);
  50:quantum/keymap_common.c **** 
  51:quantum/keymap_common.c ****     action_t action = {};
  52:quantum/keymap_common.c ****     uint8_t  action_layer, when, mod;
  53:quantum/keymap_common.c **** 
  54:quantum/keymap_common.c ****     (void)action_layer;
  55:quantum/keymap_common.c ****     (void)when;
  56:quantum/keymap_common.c ****     (void)mod;
  57:quantum/keymap_common.c **** 
  58:quantum/keymap_common.c ****     switch (keycode) {
  59:quantum/keymap_common.c ****         case KC_A ... KC_EXSEL:
  60:quantum/keymap_common.c ****         case KC_LCTRL ... KC_RGUI:
  61:quantum/keymap_common.c ****             action.code = ACTION_KEY(keycode);
  62:quantum/keymap_common.c ****             break;
  63:quantum/keymap_common.c **** #ifdef EXTRAKEY_ENABLE
  64:quantum/keymap_common.c ****         case KC_SYSTEM_POWER ... KC_SYSTEM_WAKE:
  65:quantum/keymap_common.c ****             action.code = ACTION_USAGE_SYSTEM(KEYCODE2SYSTEM(keycode));
  66:quantum/keymap_common.c ****             break;
  67:quantum/keymap_common.c ****         case KC_AUDIO_MUTE ... KC_BRIGHTNESS_DOWN:
  68:quantum/keymap_common.c ****             action.code = ACTION_USAGE_CONSUMER(KEYCODE2CONSUMER(keycode));
  69:quantum/keymap_common.c ****             break;
  70:quantum/keymap_common.c **** #endif
  71:quantum/keymap_common.c **** #ifdef MOUSEKEY_ENABLE
  72:quantum/keymap_common.c ****         case KC_MS_UP ... KC_MS_ACCEL2:
  73:quantum/keymap_common.c ****             action.code = ACTION_MOUSEKEY(keycode);
  74:quantum/keymap_common.c ****             break;
  75:quantum/keymap_common.c **** #endif
  76:quantum/keymap_common.c ****         case KC_TRNS:
  77:quantum/keymap_common.c ****             action.code = ACTION_TRANSPARENT;
  78:quantum/keymap_common.c ****             break;
  79:quantum/keymap_common.c ****         case QK_MODS ... QK_MODS_MAX:;
  80:quantum/keymap_common.c ****             // Has a modifier
  81:quantum/keymap_common.c ****             // Split it up
  82:quantum/keymap_common.c ****             action.code = ACTION_MODS_KEY(keycode >> 8, keycode & 0xFF);  // adds modifier to key
  83:quantum/keymap_common.c ****             break;
  84:quantum/keymap_common.c **** #ifndef NO_ACTION_FUNCTION
  85:quantum/keymap_common.c ****         case KC_FN0 ... KC_FN31:
  86:quantum/keymap_common.c ****             action.code = keymap_function_id_to_action(FN_INDEX(keycode));
  87:quantum/keymap_common.c ****             break;
  88:quantum/keymap_common.c ****         case QK_FUNCTION ... QK_FUNCTION_MAX:;
  89:quantum/keymap_common.c ****             // Is a shortcut for function action_layer, pull last 12bits
  90:quantum/keymap_common.c ****             // This means we have 4,096 FN macros at our disposal
  91:quantum/keymap_common.c ****             action.code = keymap_function_id_to_action((int)keycode & 0xFFF);
  92:quantum/keymap_common.c ****             break;
  93:quantum/keymap_common.c **** #endif
  94:quantum/keymap_common.c **** #ifndef NO_ACTION_MACRO
  95:quantum/keymap_common.c ****         case QK_MACRO ... QK_MACRO_MAX:
  96:quantum/keymap_common.c ****             if (keycode & 0x800)  // tap macros have upper bit set
  97:quantum/keymap_common.c ****                 action.code = ACTION_MACRO_TAP(keycode & 0xFF);
  98:quantum/keymap_common.c ****             else
  99:quantum/keymap_common.c ****                 action.code = ACTION_MACRO(keycode & 0xFF);
 100:quantum/keymap_common.c ****             break;
 101:quantum/keymap_common.c **** #endif
 102:quantum/keymap_common.c **** #ifndef NO_ACTION_LAYER
 103:quantum/keymap_common.c ****         case QK_LAYER_TAP ... QK_LAYER_TAP_MAX:
 104:quantum/keymap_common.c ****             action.code = ACTION_LAYER_TAP_KEY((keycode >> 0x8) & 0xF, keycode & 0xFF);
 105:quantum/keymap_common.c ****             break;
 106:quantum/keymap_common.c ****         case QK_TO ... QK_TO_MAX:;
 107:quantum/keymap_common.c ****             // Layer set "GOTO"
 108:quantum/keymap_common.c ****             when         = (keycode >> 0x4) & 0x3;
 109:quantum/keymap_common.c ****             action_layer = keycode & 0xF;
 110:quantum/keymap_common.c ****             action.code  = ACTION_LAYER_SET(action_layer, when);
 111:quantum/keymap_common.c ****             break;
 112:quantum/keymap_common.c ****         case QK_MOMENTARY ... QK_MOMENTARY_MAX:;
 113:quantum/keymap_common.c ****             // Momentary action_layer
 114:quantum/keymap_common.c ****             action_layer = keycode & 0xFF;
 115:quantum/keymap_common.c ****             action.code  = ACTION_LAYER_MOMENTARY(action_layer);
 116:quantum/keymap_common.c ****             break;
 117:quantum/keymap_common.c ****         case QK_DEF_LAYER ... QK_DEF_LAYER_MAX:;
 118:quantum/keymap_common.c ****             // Set default action_layer
 119:quantum/keymap_common.c ****             action_layer = keycode & 0xFF;
 120:quantum/keymap_common.c ****             action.code  = ACTION_DEFAULT_LAYER_SET(action_layer);
 121:quantum/keymap_common.c ****             break;
 122:quantum/keymap_common.c ****         case QK_TOGGLE_LAYER ... QK_TOGGLE_LAYER_MAX:;
 123:quantum/keymap_common.c ****             // Set toggle
 124:quantum/keymap_common.c ****             action_layer = keycode & 0xFF;
 125:quantum/keymap_common.c ****             action.code  = ACTION_LAYER_TOGGLE(action_layer);
 126:quantum/keymap_common.c ****             break;
 127:quantum/keymap_common.c **** #endif
 128:quantum/keymap_common.c **** #ifndef NO_ACTION_ONESHOT
 129:quantum/keymap_common.c ****         case QK_ONE_SHOT_LAYER ... QK_ONE_SHOT_LAYER_MAX:;
 130:quantum/keymap_common.c ****             // OSL(action_layer) - One-shot action_layer
 131:quantum/keymap_common.c ****             action_layer = keycode & 0xFF;
 132:quantum/keymap_common.c ****             action.code  = ACTION_LAYER_ONESHOT(action_layer);
 133:quantum/keymap_common.c ****             break;
 134:quantum/keymap_common.c ****         case QK_ONE_SHOT_MOD ... QK_ONE_SHOT_MOD_MAX:;
 135:quantum/keymap_common.c ****             // OSM(mod) - One-shot mod
 136:quantum/keymap_common.c ****             mod         = mod_config(keycode & 0xFF);
 137:quantum/keymap_common.c ****             action.code = ACTION_MODS_ONESHOT(mod);
 138:quantum/keymap_common.c ****             break;
 139:quantum/keymap_common.c **** #endif
 140:quantum/keymap_common.c **** #ifndef NO_ACTION_LAYER
 141:quantum/keymap_common.c ****         case QK_LAYER_TAP_TOGGLE ... QK_LAYER_TAP_TOGGLE_MAX:
 142:quantum/keymap_common.c ****             action.code = ACTION_LAYER_TAP_TOGGLE(keycode & 0xFF);
 143:quantum/keymap_common.c ****             break;
 144:quantum/keymap_common.c ****         case QK_LAYER_MOD ... QK_LAYER_MOD_MAX:
 145:quantum/keymap_common.c ****             mod          = mod_config(keycode & 0xF);
 146:quantum/keymap_common.c ****             action_layer = (keycode >> 4) & 0xF;
 147:quantum/keymap_common.c ****             action.code  = ACTION_LAYER_MODS(action_layer, mod);
 148:quantum/keymap_common.c ****             break;
 149:quantum/keymap_common.c **** #endif
 150:quantum/keymap_common.c **** #ifndef NO_ACTION_TAPPING
 151:quantum/keymap_common.c ****         case QK_MOD_TAP ... QK_MOD_TAP_MAX:
 152:quantum/keymap_common.c ****             mod         = mod_config((keycode >> 0x8) & 0x1F);
 153:quantum/keymap_common.c ****             action.code = ACTION_MODS_TAP_KEY(mod, keycode & 0xFF);
 154:quantum/keymap_common.c ****             break;
 155:quantum/keymap_common.c **** #endif
 156:quantum/keymap_common.c **** #ifdef SWAP_HANDS_ENABLE
 157:quantum/keymap_common.c ****         case QK_SWAP_HANDS ... QK_SWAP_HANDS_MAX:
 158:quantum/keymap_common.c ****             action.code = ACTION(ACT_SWAP_HANDS, keycode & 0xff);
 159:quantum/keymap_common.c ****             break;
 160:quantum/keymap_common.c **** #endif
 161:quantum/keymap_common.c **** 
 162:quantum/keymap_common.c ****         default:
 163:quantum/keymap_common.c ****             action.code = ACTION_NO;
 164:quantum/keymap_common.c ****             break;
 165:quantum/keymap_common.c ****     }
 166:quantum/keymap_common.c ****     return action;
 167:quantum/keymap_common.c **** }
 168:quantum/keymap_common.c **** 
 169:quantum/keymap_common.c **** __attribute__((weak)) const uint16_t PROGMEM fn_actions[] = {
 170:quantum/keymap_common.c **** 
 171:quantum/keymap_common.c **** };
 172:quantum/keymap_common.c **** 
 173:quantum/keymap_common.c **** /* Macro */
 174:quantum/keymap_common.c **** __attribute__((weak)) const macro_t *action_get_macro(keyrecord_t *record, uint8_t id, uint8_t opt)
  16               		.loc 1 174 0
  17               		.cfi_startproc
  18               	.LVL0:
  19               	/* prologue: function */
  20               	/* frame size = 0 */
  21               	/* stack size = 0 */
  22               	.L__stack_usage = 0
  23               		.loc 1 174 0
  24 0000 80E0      		ldi r24,0
  25 0002 90E0      		ldi r25,0
  26               	.LVL1:
  27 0004 0895      		ret
  28               		.cfi_endproc
  29               	.LFE21:
  31               		.section	.text.action_function,"ax",@progbits
  32               		.weak	action_function
  34               	action_function:
  35               	.LFB22:
 175:quantum/keymap_common.c **** 
 176:quantum/keymap_common.c **** /* Function */
 177:quantum/keymap_common.c **** __attribute__((weak)) void action_function(keyrecord_t *record, uint8_t id, uint8_t opt) {}
  36               		.loc 1 177 0
  37               		.cfi_startproc
  38               	.LVL2:
  39               	/* prologue: function */
  40               	/* frame size = 0 */
  41               	/* stack size = 0 */
  42               	.L__stack_usage = 0
  43 0000 0895      		ret
  44               		.cfi_endproc
  45               	.LFE22:
  47               		.section	.text.keymap_key_to_keycode,"ax",@progbits
  48               		.weak	keymap_key_to_keycode
  50               	keymap_key_to_keycode:
  51               	.LFB23:
 178:quantum/keymap_common.c **** 
 179:quantum/keymap_common.c **** // translates key to keycode
 180:quantum/keymap_common.c **** __attribute__((weak)) uint16_t keymap_key_to_keycode(uint8_t layer, keypos_t key) {
  52               		.loc 1 180 0
  53               		.cfi_startproc
  54               	.LVL3:
  55               	/* prologue: function */
  56               	/* frame size = 0 */
  57               	/* stack size = 0 */
  58               	.L__stack_usage = 0
  59               	.LBB2:
 181:quantum/keymap_common.c ****     // Read entire word (16bits)
 182:quantum/keymap_common.c ****     return pgm_read_word(&keymaps[(layer)][(key.row)][(key.col)]);
  60               		.loc 1 182 0
  61 0000 2CE3      		ldi r18,lo8(60)
  62 0002 829F      		mul r24,r18
  63 0004 C001      		movw r24,r0
  64 0006 1124      		clr __zero_reg__
  65               	.LVL4:
  66 0008 FC01      		movw r30,r24
  67 000a 26E0      		ldi r18,lo8(6)
  68 000c 729F      		mul r23,r18
  69 000e E00D      		add r30,r0
  70 0010 F11D      		adc r31,r1
  71 0012 1124      		clr __zero_reg__
  72 0014 E60F      		add r30,r22
  73 0016 F11D      		adc r31,__zero_reg__
  74 0018 EE0F      		lsl r30
  75 001a FF1F      		rol r31
  76 001c E050      		subi r30,lo8(-(keymaps))
  77 001e F040      		sbci r31,hi8(-(keymaps))
  78               	.LVL5:
  79               	/* #APP */
  80               	 ;  182 "quantum/keymap_common.c" 1
  81 0020 8591      		lpm r24, Z+
  82 0022 9491      		lpm r25, Z
  83               		
  84               	 ;  0 "" 2
  85               	.LVL6:
  86               	/* #NOAPP */
  87               	.LBE2:
 183:quantum/keymap_common.c **** }
  88               		.loc 1 183 0
  89 0024 0895      		ret
  90               		.cfi_endproc
  91               	.LFE23:
  93               		.section	.text.keymap_function_id_to_action,"ax",@progbits
  94               		.weak	keymap_function_id_to_action
  96               	keymap_function_id_to_action:
  97               	.LFB24:
 184:quantum/keymap_common.c **** 
 185:quantum/keymap_common.c **** // translates function id to action
 186:quantum/keymap_common.c **** __attribute__((weak)) uint16_t keymap_function_id_to_action(uint16_t function_id) {
  98               		.loc 1 186 0
  99               		.cfi_startproc
 100               	.LVL7:
 101               	/* prologue: function */
 102               	/* frame size = 0 */
 103               	/* stack size = 0 */
 104               	.L__stack_usage = 0
 105               	.LBB3:
 187:quantum/keymap_common.c **** // The compiler sees the empty (weak) fn_actions and generates a warning
 188:quantum/keymap_common.c **** // This function should not be called in that case, so the warning is too strict
 189:quantum/keymap_common.c **** // If this function is called however, the keymap should have overridden fn_actions, and then the c
 190:quantum/keymap_common.c **** // is comparing against the wrong array
 191:quantum/keymap_common.c **** #pragma GCC diagnostic push
 192:quantum/keymap_common.c **** #pragma GCC diagnostic ignored "-Warray-bounds"
 193:quantum/keymap_common.c ****     return pgm_read_word(&fn_actions[function_id]);
 106               		.loc 1 193 0
 107 0000 880F      		lsl r24
 108 0002 991F      		rol r25
 109               	.LVL8:
 110 0004 FC01      		movw r30,r24
 111 0006 E050      		subi r30,lo8(-(fn_actions))
 112 0008 F040      		sbci r31,hi8(-(fn_actions))
 113               	.LVL9:
 114               	/* #APP */
 115               	 ;  193 "quantum/keymap_common.c" 1
 116 000a 8591      		lpm r24, Z+
 117 000c 9491      		lpm r25, Z
 118               		
 119               	 ;  0 "" 2
 120               	.LVL10:
 121               	/* #NOAPP */
 122               	.LBE3:
 194:quantum/keymap_common.c **** #pragma GCC diagnostic pop
 195:quantum/keymap_common.c **** }
 123               		.loc 1 195 0
 124 000e 0895      		ret
 125               		.cfi_endproc
 126               	.LFE24:
 128               		.section	.text.action_for_key,"ax",@progbits
 129               	.global	action_for_key
 131               	action_for_key:
 132               	.LFB20:
  44:quantum/keymap_common.c ****     // 16bit keycodes - important
 133               		.loc 1 44 0
 134               		.cfi_startproc
 135               	.LVL11:
 136 0000 CF93      		push r28
 137               	.LCFI0:
 138               		.cfi_def_cfa_offset 3
 139               		.cfi_offset 28, -2
 140 0002 DF93      		push r29
 141               	.LCFI1:
 142               		.cfi_def_cfa_offset 4
 143               		.cfi_offset 29, -3
 144               	/* prologue: function */
 145               	/* frame size = 0 */
 146               	/* stack size = 2 */
 147               	.L__stack_usage = 2
  46:quantum/keymap_common.c **** 
 148               		.loc 1 46 0
 149 0004 0E94 0000 		call keymap_key_to_keycode
 150               	.LVL12:
  49:quantum/keymap_common.c **** 
 151               		.loc 1 49 0
 152 0008 0E94 0000 		call keycode_config
 153               	.LVL13:
 154 000c EC01      		movw r28,r24
 155               	.LVL14:
  58:quantum/keymap_common.c ****         case KC_A ... KC_EXSEL:
 156               		.loc 1 58 0
 157 000e 8115      		cp r24,__zero_reg__
 158 0010 9045      		sbci r25,80
 159 0012 00F0      		brlo .+2
 160 0014 00C0      		rjmp .L7
 161 0016 C115      		cp r28,__zero_reg__
 162 0018 80E4      		ldi r24,64
 163 001a D807      		cpc r29,r24
 164               	.LVL15:
 165 001c 00F0      		brlo .+2
 166 001e 00C0      		rjmp .L8
 167 0020 C03E      		cpi r28,-32
 168 0022 D105      		cpc r29,__zero_reg__
 169 0024 00F4      		brsh .L9
 170 0026 C03C      		cpi r28,-64
 171 0028 D105      		cpc r29,__zero_reg__
 172 002a 00F0      		brlo .+2
 173 002c 00C0      		rjmp .L10
 174 002e C83A      		cpi r28,-88
 175 0030 D105      		cpc r29,__zero_reg__
 176 0032 00F4      		brsh .L11
 177 0034 C53A      		cpi r28,-91
 178 0036 D105      		cpc r29,__zero_reg__
 179 0038 00F0      		brlo .+2
 180 003a 00C0      		rjmp .L12
 181 003c C130      		cpi r28,1
 182 003e D105      		cpc r29,__zero_reg__
 183 0040 01F4      		brne .+2
 184 0042 00C0      		rjmp .L13
 185 0044 00F4      		brsh .+2
 186 0046 00C0      		rjmp .L6
 187 0048 C430      		cpi r28,4
 188 004a D105      		cpc r29,__zero_reg__
 189 004c 00F0      		brlo .+2
 190 004e 00C0      		rjmp .L13
 191 0050 00C0      		rjmp .L6
 192               	.L11:
 193 0052 CF3B      		cpi r28,-65
 194 0054 D105      		cpc r29,__zero_reg__
 195 0056 00F0      		brlo .+2
 196 0058 00C0      		rjmp .L6
 197               	.LVL16:
 198 005a C85A      		subi r28,lo8(-(88))
 199               	.LVL17:
 200 005c D0E0      		ldi r29,0
 201 005e CC0F      		lsl r28
 202 0060 DD1F      		rol r29
 203 0062 C050      		subi r28,lo8(-(CSWTCH.3))
 204 0064 D040      		sbci r29,hi8(-(CSWTCH.3))
 205 0066 0990      		ld __tmp_reg__,Y+
 206 0068 D881      		ld r29,Y
 207 006a C02D      		mov r28,__tmp_reg__
 208               	.LVL18:
  68:quantum/keymap_common.c ****             break;
 209               		.loc 1 68 0
 210 006c D464      		ori r29,68
 211               	.LVL19:
  69:quantum/keymap_common.c **** #endif
 212               		.loc 1 69 0
 213 006e 00C0      		rjmp .L13
 214               	.LVL20:
 215               	.L9:
  58:quantum/keymap_common.c ****         case KC_A ... KC_EXSEL:
 216               		.loc 1 58 0
 217 0070 C115      		cp r28,__zero_reg__
 218 0072 90E2      		ldi r25,32
 219 0074 D907      		cpc r29,r25
 220 0076 00F4      		brsh .L15
 221 0078 C115      		cp r28,__zero_reg__
 222 007a 21E0      		ldi r18,1
 223 007c D207      		cpc r29,r18
 224 007e 00F0      		brlo .+2
 225 0080 00C0      		rjmp .L13
 226 0082 C83E      		cpi r28,-24
 227 0084 D105      		cpc r29,__zero_reg__
 228 0086 00F4      		brsh .+2
 229 0088 00C0      		rjmp .L13
 230 008a 00C0      		rjmp .L6
 231               	.L15:
 232 008c C115      		cp r28,__zero_reg__
 233 008e 80E3      		ldi r24,48
 234 0090 D807      		cpc r29,r24
 235 0092 00F0      		brlo .+2
 236 0094 00C0      		rjmp .L35
  91:quantum/keymap_common.c ****             break;
 237               		.loc 1 91 0
 238 0096 CE01      		movw r24,r28
 239 0098 9F70      		andi r25,15
 240 009a 00C0      		rjmp .L40
 241               	.LVL21:
 242               	.L7:
  58:quantum/keymap_common.c ****         case KC_A ... KC_EXSEL:
 243               		.loc 1 58 0
 244 009c C115      		cp r28,__zero_reg__
 245 009e 95E5      		ldi r25,85
 246 00a0 D907      		cpc r29,r25
 247 00a2 00F0      		brlo .+2
 248 00a4 00C0      		rjmp .L18
 249 00a6 C115      		cp r28,__zero_reg__
 250 00a8 24E5      		ldi r18,84
 251 00aa D207      		cpc r29,r18
 252 00ac 00F0      		brlo .+2
 253 00ae 00C0      		rjmp .L19
 254 00b0 C115      		cp r28,__zero_reg__
 255 00b2 82E5      		ldi r24,82
 256 00b4 D807      		cpc r29,r24
 257               	.LVL22:
 258 00b6 00F4      		brsh .L20
 259 00b8 C115      		cp r28,__zero_reg__
 260 00ba 91E5      		ldi r25,81
 261 00bc D907      		cpc r29,r25
 262 00be 00F0      		brlo .+2
 263 00c0 00C0      		rjmp .L42
 264               	.LVL23:
 110:quantum/keymap_common.c ****             break;
 265               		.loc 1 110 0
 266 00c2 CE01      		movw r24,r28
 267 00c4 74E0      		ldi r23,4
 268               		1:
 269 00c6 9695      		lsr r25
 270 00c8 8795      		ror r24
 271 00ca 7A95      		dec r23
 272 00cc 01F4      		brne 1b
 273 00ce 8370      		andi r24,lo8(3)
 274 00d0 9C2F      		mov r25,r28
 275 00d2 9370      		andi r25,lo8(3)
 276 00d4 21E0      		ldi r18,lo8(1)
 277 00d6 30E0      		ldi r19,0
 278 00d8 00C0      		rjmp 2f
 279               		1:
 280 00da 220F      		lsl r18
 281 00dc 331F      		rol r19
 282               		2:
 283 00de 9A95      		dec r25
 284 00e0 02F4      		brpl 1b
 285 00e2 3C68      		ori r19,140
 286 00e4 382B      		or r19,r24
 287 00e6 CF70      		andi r28,lo8(15)
 288               	.LVL24:
 289 00e8 C695      		lsr r28
 290 00ea C695      		lsr r28
 291 00ec 80E2      		ldi r24,lo8(32)
 292 00ee C89F      		mul r28,r24
 293 00f0 E001      		movw r28,r0
 294 00f2 1124      		clr __zero_reg__
 295               	.LVL25:
 296 00f4 C22B      		or r28,r18
 297 00f6 D32B      		or r29,r19
 298               	.LVL26:
 111:quantum/keymap_common.c ****         case QK_MOMENTARY ... QK_MOMENTARY_MAX:;
 299               		.loc 1 111 0
 300 00f8 00C0      		rjmp .L13
 301               	.LVL27:
 302               	.L20:
 303 00fa 9E01      		movw r18,r28
 304 00fc 2370      		andi r18,3
 305 00fe 3327      		clr r19
 306 0100 8C2F      		mov r24,r28
 307 0102 8695      		lsr r24
 308 0104 8695      		lsr r24
  58:quantum/keymap_common.c ****         case KC_A ... KC_EXSEL:
 309               		.loc 1 58 0
 310 0106 C115      		cp r28,__zero_reg__
 311 0108 D345      		sbci r29,83
 312 010a 00F0      		brlo .+2
 313 010c 00C0      		rjmp .L37
 314               	.LVL28:
 120:quantum/keymap_common.c ****             break;
 315               		.loc 1 120 0
 316 010e C1E0      		ldi r28,lo8(1)
 317 0110 D0E0      		ldi r29,0
 318               	.LVL29:
 319 0112 00C0      		rjmp 2f
 320               		1:
 321 0114 CC0F      		lsl r28
 322 0116 DD1F      		rol r29
 323               		2:
 324 0118 2A95      		dec r18
 325 011a 02F4      		brpl 1b
 326 011c 20E2      		ldi r18,lo8(32)
 327 011e 829F      		mul r24,r18
 328 0120 C001      		movw r24,r0
 329 0122 1124      		clr __zero_reg__
 330 0124 C82B      		or r28,r24
 331 0126 D92B      		or r29,r25
 332               	.LVL30:
 333 0128 DC68      		ori r29,140
 334               	.LVL31:
 121:quantum/keymap_common.c ****         case QK_TOGGLE_LAYER ... QK_TOGGLE_LAYER_MAX:;
 335               		.loc 1 121 0
 336 012a 00C0      		rjmp .L13
 337               	.LVL32:
 338               	.L18:
  58:quantum/keymap_common.c ****         case KC_A ... KC_EXSEL:
 339               		.loc 1 58 0
 340 012c C115      		cp r28,__zero_reg__
 341 012e 29E5      		ldi r18,89
 342 0130 D207      		cpc r29,r18
 343 0132 00F4      		brsh .L25
 344 0134 C115      		cp r28,__zero_reg__
 345 0136 88E5      		ldi r24,88
 346 0138 D807      		cpc r29,r24
 347               	.LVL33:
 348 013a 00F0      		brlo .+2
 349 013c 00C0      		rjmp .L26
 350 013e C115      		cp r28,__zero_reg__
 351 0140 96E5      		ldi r25,86
 352 0142 D907      		cpc r29,r25
 353 0144 00F0      		brlo .+2
 354 0146 00C0      		rjmp .L6
 136:quantum/keymap_common.c ****             action.code = ACTION_MODS_ONESHOT(mod);
 355               		.loc 1 136 0
 356 0148 8C2F      		mov r24,r28
 357 014a 0E94 0000 		call mod_config
 358               	.LVL34:
 137:quantum/keymap_common.c ****             break;
 359               		.loc 1 137 0
 360 014e 8F71      		andi r24,lo8(31)
 361               	.LVL35:
 362 0150 C82F      		mov r28,r24
 363               	.LVL36:
 364 0152 D0E0      		ldi r29,0
 365 0154 DC2F      		mov r29,r28
 366 0156 CC27      		clr r28
 367               	.LVL37:
 368 0158 D062      		ori r29,32
 369               	.LVL38:
 138:quantum/keymap_common.c **** #endif
 370               		.loc 1 138 0
 371 015a 00C0      		rjmp .L13
 372               	.LVL39:
 373               	.L25:
  58:quantum/keymap_common.c ****         case KC_A ... KC_EXSEL:
 374               		.loc 1 58 0
 375 015c C115      		cp r28,__zero_reg__
 376 015e 2AE5      		ldi r18,90
 377 0160 D207      		cpc r29,r18
 378 0162 00F4      		brsh .+2
 379 0164 00C0      		rjmp .L28
 380 0166 CE01      		movw r24,r28
 381               	.LVL40:
 382 0168 9056      		subi r25,96
 383 016a 8115      		cp r24,__zero_reg__
 384 016c 9042      		sbci r25,32
 385 016e 00F0      		brlo .+2
 386 0170 00C0      		rjmp .L6
 152:quantum/keymap_common.c ****             action.code = ACTION_MODS_TAP_KEY(mod, keycode & 0xFF);
 387               		.loc 1 152 0
 388 0172 8D2F      		mov r24,r29
 389 0174 8F71      		andi r24,lo8(31)
 390 0176 0E94 0000 		call mod_config
 391               	.LVL41:
 392 017a DD27      		clr r29
 393               	.LVL42:
 394 017c D062      		ori r29,32
 153:quantum/keymap_common.c ****             break;
 395               		.loc 1 153 0
 396 017e 8F71      		andi r24,lo8(31)
 397               	.LVL43:
 398 0180 D82B      		or r29,r24
 399               	.LVL44:
 154:quantum/keymap_common.c **** #endif
 400               		.loc 1 154 0
 401 0182 00C0      		rjmp .L13
 402               	.LVL45:
 403               	.L12:
 404 0184 C55A      		subi r28,lo8(-(91))
 405               	.LVL46:
 406 0186 D0E0      		ldi r29,0
 407 0188 C050      		subi r28,lo8(-(CSWTCH.1))
 408 018a D040      		sbci r29,hi8(-(CSWTCH.1))
 409 018c C881      		ld r28,Y
 410 018e D0E0      		ldi r29,0
 411               	.LVL47:
  65:quantum/keymap_common.c ****             break;
 412               		.loc 1 65 0
 413 0190 D064      		ori r29,64
 414               	.LVL48:
  66:quantum/keymap_common.c ****         case KC_AUDIO_MUTE ... KC_BRIGHTNESS_DOWN:
 415               		.loc 1 66 0
 416 0192 00C0      		rjmp .L13
 417               	.LVL49:
 418               	.L10:
  86:quantum/keymap_common.c ****             break;
 419               		.loc 1 86 0
 420 0194 CE01      		movw r24,r28
 421 0196 805C      		subi r24,-64
 422 0198 9109      		sbc r25,__zero_reg__
 423               	.L40:
  91:quantum/keymap_common.c ****             break;
 424               		.loc 1 91 0
 425 019a 0E94 0000 		call keymap_function_id_to_action
 426               	.LVL50:
 427 019e EC01      		movw r28,r24
 428               	.LVL51:
  92:quantum/keymap_common.c **** #endif
 429               		.loc 1 92 0
 430 01a0 00C0      		rjmp .L13
 431               	.LVL52:
 432               	.L35:
 433 01a2 CE01      		movw r24,r28
 434 01a4 9927      		clr r25
  96:quantum/keymap_common.c ****                 action.code = ACTION_MACRO_TAP(keycode & 0xFF);
 435               		.loc 1 96 0
 436 01a6 D3FF      		sbrs r29,3
 437 01a8 00C0      		rjmp .L30
 438               	.LVL53:
  97:quantum/keymap_common.c ****             else
 439               		.loc 1 97 0
 440 01aa EC01      		movw r28,r24
 441 01ac D86C      		ori r29,200
 442               	.LVL54:
 443 01ae 00C0      		rjmp .L13
 444               	.LVL55:
 445               	.L30:
  99:quantum/keymap_common.c ****             break;
 446               		.loc 1 99 0
 447 01b0 EC01      		movw r28,r24
 448 01b2 D06C      		ori r29,192
 449               	.LVL56:
 450 01b4 00C0      		rjmp .L13
 451               	.LVL57:
 452               	.L8:
 104:quantum/keymap_common.c ****             break;
 453               		.loc 1 104 0
 454 01b6 DF70      		andi r29,15
 455               	.LVL58:
 456 01b8 D06A      		ori r29,160
 457               	.LVL59:
 105:quantum/keymap_common.c ****         case QK_TO ... QK_TO_MAX:;
 458               		.loc 1 105 0
 459 01ba 00C0      		rjmp .L13
 460               	.LVL60:
 461               	.L42:
 115:quantum/keymap_common.c ****             break;
 462               		.loc 1 115 0
 463 01bc DC2F      		mov r29,r28
 464 01be CC27      		clr r28
 465               	.LVL61:
 466 01c0 C16F      		ori r28,241
 467 01c2 D06A      		ori r29,160
 468               	.LVL62:
 116:quantum/keymap_common.c ****         case QK_DEF_LAYER ... QK_DEF_LAYER_MAX:;
 469               		.loc 1 116 0
 470 01c4 00C0      		rjmp .L13
 471               	.LVL63:
 472               	.L37:
 125:quantum/keymap_common.c ****             break;
 473               		.loc 1 125 0
 474 01c6 C1E0      		ldi r28,lo8(1)
 475 01c8 D0E0      		ldi r29,0
 476               	.LVL64:
 477 01ca 00C0      		rjmp 2f
 478               		1:
 479 01cc CC0F      		lsl r28
 480 01ce DD1F      		rol r29
 481               		2:
 482 01d0 2A95      		dec r18
 483 01d2 02F4      		brpl 1b
 484 01d4 20E2      		ldi r18,lo8(32)
 485 01d6 829F      		mul r24,r18
 486 01d8 C001      		movw r24,r0
 487 01da 1124      		clr __zero_reg__
 488 01dc C82B      		or r28,r24
 489 01de D92B      		or r29,r25
 490               	.LVL65:
 491 01e0 DA68      		ori r29,138
 492               	.LVL66:
 126:quantum/keymap_common.c **** #endif
 493               		.loc 1 126 0
 494 01e2 00C0      		rjmp .L13
 495               	.LVL67:
 496               	.L19:
 132:quantum/keymap_common.c ****             break;
 497               		.loc 1 132 0
 498 01e4 DC2F      		mov r29,r28
 499 01e6 CC27      		clr r28
 500               	.LVL68:
 501 01e8 C46F      		ori r28,244
 502 01ea D06A      		ori r29,160
 503               	.LVL69:
 133:quantum/keymap_common.c ****         case QK_ONE_SHOT_MOD ... QK_ONE_SHOT_MOD_MAX:;
 504               		.loc 1 133 0
 505 01ec 00C0      		rjmp .L13
 506               	.LVL70:
 507               	.L26:
 142:quantum/keymap_common.c ****             break;
 508               		.loc 1 142 0
 509 01ee DC2F      		mov r29,r28
 510 01f0 CC27      		clr r28
 511               	.LVL71:
 512 01f2 C06F      		ori r28,240
 513 01f4 D06A      		ori r29,160
 514               	.LVL72:
 143:quantum/keymap_common.c ****         case QK_LAYER_MOD ... QK_LAYER_MOD_MAX:
 515               		.loc 1 143 0
 516 01f6 00C0      		rjmp .L13
 517               	.LVL73:
 518               	.L28:
 145:quantum/keymap_common.c ****             action_layer = (keycode >> 4) & 0xF;
 519               		.loc 1 145 0
 520 01f8 8C2F      		mov r24,r28
 521               	.LVL74:
 522 01fa 8F70      		andi r24,lo8(15)
 523 01fc 0E94 0000 		call mod_config
 524               	.LVL75:
 147:quantum/keymap_common.c ****             break;
 525               		.loc 1 147 0
 526 0200 94E0      		ldi r25,4
 527               		1:
 528 0202 D695      		lsr r29
 529 0204 C795      		ror r28
 530 0206 9A95      		dec r25
 531 0208 01F4      		brne 1b
 532               	.LVL76:
 533 020a CF70      		andi r28,lo8(15)
 534 020c D0E0      		ldi r29,0
 535 020e DC2F      		mov r29,r28
 536 0210 CC27      		clr r28
 537 0212 D069      		ori r29,144
 538               	.LVL77:
 539 0214 C82B      		or r28,r24
 540               	.LVL78:
 148:quantum/keymap_common.c **** #endif
 541               		.loc 1 148 0
 542 0216 00C0      		rjmp .L13
 543               	.LVL79:
 544               	.L6:
 163:quantum/keymap_common.c ****             break;
 545               		.loc 1 163 0
 546 0218 C0E0      		ldi r28,0
 547 021a D0E0      		ldi r29,0
 548               	.LVL80:
 549               	.L13:
 167:quantum/keymap_common.c **** 
 550               		.loc 1 167 0
 551 021c CE01      		movw r24,r28
 552               	/* epilogue start */
 553 021e DF91      		pop r29
 554 0220 CF91      		pop r28
 555 0222 0895      		ret
 556               		.cfi_endproc
 557               	.LFE20:
 559               		.section	.rodata.CSWTCH.3,"a",@progbits
 562               	CSWTCH.3:
 563 0000 E200      		.word	226
 564 0002 E900      		.word	233
 565 0004 EA00      		.word	234
 566 0006 B500      		.word	181
 567 0008 B600      		.word	182
 568 000a B700      		.word	183
 569 000c CD00      		.word	205
 570 000e 8301      		.word	387
 571 0010 CC00      		.word	204
 572 0012 8A01      		.word	394
 573 0014 9201      		.word	402
 574 0016 9401      		.word	404
 575 0018 2102      		.word	545
 576 001a 2302      		.word	547
 577 001c 2402      		.word	548
 578 001e 2502      		.word	549
 579 0020 2602      		.word	550
 580 0022 2702      		.word	551
 581 0024 2A02      		.word	554
 582 0026 B300      		.word	179
 583 0028 B400      		.word	180
 584 002a 6F00      		.word	111
 585 002c 7000      		.word	112
 586               		.section	.rodata.CSWTCH.1,"a",@progbits
 589               	CSWTCH.1:
 590 0000 81        		.byte	-127
 591 0001 82        		.byte	-126
 592 0002 83        		.byte	-125
 593               		.weak	fn_actions
 594               		.section	.progmem.data.fn_actions,"a",@progbits
 597               	fn_actions:
 598               		.text
 599               	.Letext0:
 600               		.file 2 "/usr/lib/avr/include/stdint.h"
 601               		.file 3 "tmk_core/common/keyboard.h"
 602               		.file 4 "tmk_core/common/keycode.h"
 603               		.file 5 "tmk_core/common/action_code.h"
 604               		.file 6 "tmk_core/common/action_macro.h"
 605               		.file 7 "tmk_core/common/action.h"
 606               		.file 8 "tmk_core/common/report.h"
 607               		.file 9 "quantum/quantum_keycodes.h"
 608               		.file 10 "quantum/keymap.h"
 609               		.file 11 "tmk_core/common/action_util.h"
 610               		.file 12 "quantum/keycode_config.h"
DEFINED SYMBOLS
                            *ABS*:0000000000000000 keymap_common.c
     /tmp/ccsT4XUx.s:2      *ABS*:000000000000003e __SP_H__
     /tmp/ccsT4XUx.s:3      *ABS*:000000000000003d __SP_L__
     /tmp/ccsT4XUx.s:4      *ABS*:000000000000003f __SREG__
     /tmp/ccsT4XUx.s:5      *ABS*:0000000000000000 __tmp_reg__
     /tmp/ccsT4XUx.s:6      *ABS*:0000000000000001 __zero_reg__
     /tmp/ccsT4XUx.s:13     .text.action_get_macro:0000000000000000 action_get_macro
     /tmp/ccsT4XUx.s:34     .text.action_function:0000000000000000 action_function
     /tmp/ccsT4XUx.s:50     .text.keymap_key_to_keycode:0000000000000000 keymap_key_to_keycode
     /tmp/ccsT4XUx.s:96     .text.keymap_function_id_to_action:0000000000000000 keymap_function_id_to_action
     /tmp/ccsT4XUx.s:597    .progmem.data.fn_actions:0000000000000000 fn_actions
     /tmp/ccsT4XUx.s:131    .text.action_for_key:0000000000000000 action_for_key
     /tmp/ccsT4XUx.s:562    .rodata.CSWTCH.3:0000000000000000 CSWTCH.3
     /tmp/ccsT4XUx.s:589    .rodata.CSWTCH.1:0000000000000000 CSWTCH.1

UNDEFINED SYMBOLS
keymaps
keycode_config
mod_config
__do_copy_data
