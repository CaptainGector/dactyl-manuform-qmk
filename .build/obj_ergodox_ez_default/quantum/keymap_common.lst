   1               		.file	"keymap_common.c"
   2               	__SP_H__ = 0x3e
   3               	__SP_L__ = 0x3d
   4               	__SREG__ = 0x3f
   5               	__tmp_reg__ = 0
   6               	__zero_reg__ = 1
   7               		.text
   8               	.Ltext0:
   9               		.cfi_sections	.debug_frame
  10               		.section	.text.action_get_macro,"ax",@progbits
  11               		.weak	action_get_macro
  13               	action_get_macro:
  14               	.LFB113:
  15               		.file 1 "quantum/keymap_common.c"
   1:quantum/keymap_common.c **** /*
   2:quantum/keymap_common.c **** Copyright 2012-2017 Jun Wako <wakojun@gmail.com>
   3:quantum/keymap_common.c **** 
   4:quantum/keymap_common.c **** This program is free software: you can redistribute it and/or modify
   5:quantum/keymap_common.c **** it under the terms of the GNU General Public License as published by
   6:quantum/keymap_common.c **** the Free Software Foundation, either version 2 of the License, or
   7:quantum/keymap_common.c **** (at your option) any later version.
   8:quantum/keymap_common.c **** 
   9:quantum/keymap_common.c **** This program is distributed in the hope that it will be useful,
  10:quantum/keymap_common.c **** but WITHOUT ANY WARRANTY; without even the implied warranty of
  11:quantum/keymap_common.c **** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  12:quantum/keymap_common.c **** GNU General Public License for more details.
  13:quantum/keymap_common.c **** 
  14:quantum/keymap_common.c **** You should have received a copy of the GNU General Public License
  15:quantum/keymap_common.c **** along with this program.  If not, see <http://www.gnu.org/licenses/>.
  16:quantum/keymap_common.c **** */
  17:quantum/keymap_common.c **** 
  18:quantum/keymap_common.c **** #include "keymap.h"
  19:quantum/keymap_common.c **** #include "report.h"
  20:quantum/keymap_common.c **** #include "keycode.h"
  21:quantum/keymap_common.c **** #include "action_layer.h"
  22:quantum/keymap_common.c **** #if defined(__AVR__)
  23:quantum/keymap_common.c **** #    include <util/delay.h>
  24:quantum/keymap_common.c **** #    include <stdio.h>
  25:quantum/keymap_common.c **** #endif
  26:quantum/keymap_common.c **** #include "action.h"
  27:quantum/keymap_common.c **** #include "action_macro.h"
  28:quantum/keymap_common.c **** #include "debug.h"
  29:quantum/keymap_common.c **** #include "quantum.h"
  30:quantum/keymap_common.c **** 
  31:quantum/keymap_common.c **** #ifdef BACKLIGHT_ENABLE
  32:quantum/keymap_common.c **** #    include "backlight.h"
  33:quantum/keymap_common.c **** #endif
  34:quantum/keymap_common.c **** 
  35:quantum/keymap_common.c **** #ifdef MIDI_ENABLE
  36:quantum/keymap_common.c **** #    include "process_midi.h"
  37:quantum/keymap_common.c **** #endif
  38:quantum/keymap_common.c **** 
  39:quantum/keymap_common.c **** extern keymap_config_t keymap_config;
  40:quantum/keymap_common.c **** 
  41:quantum/keymap_common.c **** #include <inttypes.h>
  42:quantum/keymap_common.c **** 
  43:quantum/keymap_common.c **** /* converts key to action */
  44:quantum/keymap_common.c **** action_t action_for_key(uint8_t layer, keypos_t key) {
  45:quantum/keymap_common.c ****     // 16bit keycodes - important
  46:quantum/keymap_common.c ****     uint16_t keycode = keymap_key_to_keycode(layer, key);
  47:quantum/keymap_common.c **** 
  48:quantum/keymap_common.c ****     // keycode remapping
  49:quantum/keymap_common.c ****     keycode = keycode_config(keycode);
  50:quantum/keymap_common.c **** 
  51:quantum/keymap_common.c ****     action_t action = {};
  52:quantum/keymap_common.c ****     uint8_t  action_layer, when, mod;
  53:quantum/keymap_common.c **** 
  54:quantum/keymap_common.c ****     (void)action_layer;
  55:quantum/keymap_common.c ****     (void)when;
  56:quantum/keymap_common.c ****     (void)mod;
  57:quantum/keymap_common.c **** 
  58:quantum/keymap_common.c ****     switch (keycode) {
  59:quantum/keymap_common.c ****         case KC_A ... KC_EXSEL:
  60:quantum/keymap_common.c ****         case KC_LCTRL ... KC_RGUI:
  61:quantum/keymap_common.c ****             action.code = ACTION_KEY(keycode);
  62:quantum/keymap_common.c ****             break;
  63:quantum/keymap_common.c **** #ifdef EXTRAKEY_ENABLE
  64:quantum/keymap_common.c ****         case KC_SYSTEM_POWER ... KC_SYSTEM_WAKE:
  65:quantum/keymap_common.c ****             action.code = ACTION_USAGE_SYSTEM(KEYCODE2SYSTEM(keycode));
  66:quantum/keymap_common.c ****             break;
  67:quantum/keymap_common.c ****         case KC_AUDIO_MUTE ... KC_BRIGHTNESS_DOWN:
  68:quantum/keymap_common.c ****             action.code = ACTION_USAGE_CONSUMER(KEYCODE2CONSUMER(keycode));
  69:quantum/keymap_common.c ****             break;
  70:quantum/keymap_common.c **** #endif
  71:quantum/keymap_common.c **** #ifdef MOUSEKEY_ENABLE
  72:quantum/keymap_common.c ****         case KC_MS_UP ... KC_MS_ACCEL2:
  73:quantum/keymap_common.c ****             action.code = ACTION_MOUSEKEY(keycode);
  74:quantum/keymap_common.c ****             break;
  75:quantum/keymap_common.c **** #endif
  76:quantum/keymap_common.c ****         case KC_TRNS:
  77:quantum/keymap_common.c ****             action.code = ACTION_TRANSPARENT;
  78:quantum/keymap_common.c ****             break;
  79:quantum/keymap_common.c ****         case QK_MODS ... QK_MODS_MAX:;
  80:quantum/keymap_common.c ****             // Has a modifier
  81:quantum/keymap_common.c ****             // Split it up
  82:quantum/keymap_common.c ****             action.code = ACTION_MODS_KEY(keycode >> 8, keycode & 0xFF);  // adds modifier to key
  83:quantum/keymap_common.c ****             break;
  84:quantum/keymap_common.c **** #ifndef NO_ACTION_FUNCTION
  85:quantum/keymap_common.c ****         case KC_FN0 ... KC_FN31:
  86:quantum/keymap_common.c ****             action.code = keymap_function_id_to_action(FN_INDEX(keycode));
  87:quantum/keymap_common.c ****             break;
  88:quantum/keymap_common.c ****         case QK_FUNCTION ... QK_FUNCTION_MAX:;
  89:quantum/keymap_common.c ****             // Is a shortcut for function action_layer, pull last 12bits
  90:quantum/keymap_common.c ****             // This means we have 4,096 FN macros at our disposal
  91:quantum/keymap_common.c ****             action.code = keymap_function_id_to_action((int)keycode & 0xFFF);
  92:quantum/keymap_common.c ****             break;
  93:quantum/keymap_common.c **** #endif
  94:quantum/keymap_common.c **** #ifndef NO_ACTION_MACRO
  95:quantum/keymap_common.c ****         case QK_MACRO ... QK_MACRO_MAX:
  96:quantum/keymap_common.c ****             if (keycode & 0x800)  // tap macros have upper bit set
  97:quantum/keymap_common.c ****                 action.code = ACTION_MACRO_TAP(keycode & 0xFF);
  98:quantum/keymap_common.c ****             else
  99:quantum/keymap_common.c ****                 action.code = ACTION_MACRO(keycode & 0xFF);
 100:quantum/keymap_common.c ****             break;
 101:quantum/keymap_common.c **** #endif
 102:quantum/keymap_common.c **** #ifndef NO_ACTION_LAYER
 103:quantum/keymap_common.c ****         case QK_LAYER_TAP ... QK_LAYER_TAP_MAX:
 104:quantum/keymap_common.c ****             action.code = ACTION_LAYER_TAP_KEY((keycode >> 0x8) & 0xF, keycode & 0xFF);
 105:quantum/keymap_common.c ****             break;
 106:quantum/keymap_common.c ****         case QK_TO ... QK_TO_MAX:;
 107:quantum/keymap_common.c ****             // Layer set "GOTO"
 108:quantum/keymap_common.c ****             when         = (keycode >> 0x4) & 0x3;
 109:quantum/keymap_common.c ****             action_layer = keycode & 0xF;
 110:quantum/keymap_common.c ****             action.code  = ACTION_LAYER_SET(action_layer, when);
 111:quantum/keymap_common.c ****             break;
 112:quantum/keymap_common.c ****         case QK_MOMENTARY ... QK_MOMENTARY_MAX:;
 113:quantum/keymap_common.c ****             // Momentary action_layer
 114:quantum/keymap_common.c ****             action_layer = keycode & 0xFF;
 115:quantum/keymap_common.c ****             action.code  = ACTION_LAYER_MOMENTARY(action_layer);
 116:quantum/keymap_common.c ****             break;
 117:quantum/keymap_common.c ****         case QK_DEF_LAYER ... QK_DEF_LAYER_MAX:;
 118:quantum/keymap_common.c ****             // Set default action_layer
 119:quantum/keymap_common.c ****             action_layer = keycode & 0xFF;
 120:quantum/keymap_common.c ****             action.code  = ACTION_DEFAULT_LAYER_SET(action_layer);
 121:quantum/keymap_common.c ****             break;
 122:quantum/keymap_common.c ****         case QK_TOGGLE_LAYER ... QK_TOGGLE_LAYER_MAX:;
 123:quantum/keymap_common.c ****             // Set toggle
 124:quantum/keymap_common.c ****             action_layer = keycode & 0xFF;
 125:quantum/keymap_common.c ****             action.code  = ACTION_LAYER_TOGGLE(action_layer);
 126:quantum/keymap_common.c ****             break;
 127:quantum/keymap_common.c **** #endif
 128:quantum/keymap_common.c **** #ifndef NO_ACTION_ONESHOT
 129:quantum/keymap_common.c ****         case QK_ONE_SHOT_LAYER ... QK_ONE_SHOT_LAYER_MAX:;
 130:quantum/keymap_common.c ****             // OSL(action_layer) - One-shot action_layer
 131:quantum/keymap_common.c ****             action_layer = keycode & 0xFF;
 132:quantum/keymap_common.c ****             action.code  = ACTION_LAYER_ONESHOT(action_layer);
 133:quantum/keymap_common.c ****             break;
 134:quantum/keymap_common.c ****         case QK_ONE_SHOT_MOD ... QK_ONE_SHOT_MOD_MAX:;
 135:quantum/keymap_common.c ****             // OSM(mod) - One-shot mod
 136:quantum/keymap_common.c ****             mod         = mod_config(keycode & 0xFF);
 137:quantum/keymap_common.c ****             action.code = ACTION_MODS_ONESHOT(mod);
 138:quantum/keymap_common.c ****             break;
 139:quantum/keymap_common.c **** #endif
 140:quantum/keymap_common.c **** #ifndef NO_ACTION_LAYER
 141:quantum/keymap_common.c ****         case QK_LAYER_TAP_TOGGLE ... QK_LAYER_TAP_TOGGLE_MAX:
 142:quantum/keymap_common.c ****             action.code = ACTION_LAYER_TAP_TOGGLE(keycode & 0xFF);
 143:quantum/keymap_common.c ****             break;
 144:quantum/keymap_common.c ****         case QK_LAYER_MOD ... QK_LAYER_MOD_MAX:
 145:quantum/keymap_common.c ****             mod          = mod_config(keycode & 0xF);
 146:quantum/keymap_common.c ****             action_layer = (keycode >> 4) & 0xF;
 147:quantum/keymap_common.c ****             action.code  = ACTION_LAYER_MODS(action_layer, mod);
 148:quantum/keymap_common.c ****             break;
 149:quantum/keymap_common.c **** #endif
 150:quantum/keymap_common.c **** #ifndef NO_ACTION_TAPPING
 151:quantum/keymap_common.c ****         case QK_MOD_TAP ... QK_MOD_TAP_MAX:
 152:quantum/keymap_common.c ****             mod         = mod_config((keycode >> 0x8) & 0x1F);
 153:quantum/keymap_common.c ****             action.code = ACTION_MODS_TAP_KEY(mod, keycode & 0xFF);
 154:quantum/keymap_common.c ****             break;
 155:quantum/keymap_common.c **** #endif
 156:quantum/keymap_common.c **** #ifdef SWAP_HANDS_ENABLE
 157:quantum/keymap_common.c ****         case QK_SWAP_HANDS ... QK_SWAP_HANDS_MAX:
 158:quantum/keymap_common.c ****             action.code = ACTION(ACT_SWAP_HANDS, keycode & 0xff);
 159:quantum/keymap_common.c ****             break;
 160:quantum/keymap_common.c **** #endif
 161:quantum/keymap_common.c **** 
 162:quantum/keymap_common.c ****         default:
 163:quantum/keymap_common.c ****             action.code = ACTION_NO;
 164:quantum/keymap_common.c ****             break;
 165:quantum/keymap_common.c ****     }
 166:quantum/keymap_common.c ****     return action;
 167:quantum/keymap_common.c **** }
 168:quantum/keymap_common.c **** 
 169:quantum/keymap_common.c **** __attribute__((weak)) const uint16_t PROGMEM fn_actions[] = {
 170:quantum/keymap_common.c **** 
 171:quantum/keymap_common.c **** };
 172:quantum/keymap_common.c **** 
 173:quantum/keymap_common.c **** /* Macro */
 174:quantum/keymap_common.c **** __attribute__((weak)) const macro_t *action_get_macro(keyrecord_t *record, uint8_t id, uint8_t opt)
  16               		.loc 1 174 0
  17               		.cfi_startproc
  18               	.LVL0:
  19               	/* prologue: function */
  20               	/* frame size = 0 */
  21               	/* stack size = 0 */
  22               	.L__stack_usage = 0
  23               		.loc 1 174 0
  24 0000 80E0      		ldi r24,0
  25 0002 90E0      		ldi r25,0
  26               	.LVL1:
  27 0004 0895      		ret
  28               		.cfi_endproc
  29               	.LFE113:
  31               		.section	.text.action_function,"ax",@progbits
  32               		.weak	action_function
  34               	action_function:
  35               	.LFB114:
 175:quantum/keymap_common.c **** 
 176:quantum/keymap_common.c **** /* Function */
 177:quantum/keymap_common.c **** __attribute__((weak)) void action_function(keyrecord_t *record, uint8_t id, uint8_t opt) {}
  36               		.loc 1 177 0
  37               		.cfi_startproc
  38               	.LVL2:
  39               	/* prologue: function */
  40               	/* frame size = 0 */
  41               	/* stack size = 0 */
  42               	.L__stack_usage = 0
  43 0000 0895      		ret
  44               		.cfi_endproc
  45               	.LFE114:
  47               		.section	.text.keymap_key_to_keycode,"ax",@progbits
  48               		.weak	keymap_key_to_keycode
  50               	keymap_key_to_keycode:
  51               	.LFB115:
 178:quantum/keymap_common.c **** 
 179:quantum/keymap_common.c **** // translates key to keycode
 180:quantum/keymap_common.c **** __attribute__((weak)) uint16_t keymap_key_to_keycode(uint8_t layer, keypos_t key) {
  52               		.loc 1 180 0
  53               		.cfi_startproc
  54               	.LVL3:
  55               	/* prologue: function */
  56               	/* frame size = 0 */
  57               	/* stack size = 0 */
  58               	.L__stack_usage = 0
  59               	.LBB2:
 181:quantum/keymap_common.c ****     // Read entire word (16bits)
 182:quantum/keymap_common.c ****     return pgm_read_word(&keymaps[(layer)][(key.row)][(key.col)]);
  60               		.loc 1 182 0
  61 0000 24E5      		ldi r18,lo8(84)
  62 0002 829F      		mul r24,r18
  63 0004 C001      		movw r24,r0
  64 0006 1124      		clr __zero_reg__
  65               	.LVL4:
  66 0008 FC01      		movw r30,r24
  67 000a 26E0      		ldi r18,lo8(6)
  68 000c 729F      		mul r23,r18
  69 000e E00D      		add r30,r0
  70 0010 F11D      		adc r31,r1
  71 0012 1124      		clr __zero_reg__
  72 0014 E60F      		add r30,r22
  73 0016 F11D      		adc r31,__zero_reg__
  74 0018 EE0F      		lsl r30
  75 001a FF1F      		rol r31
  76 001c E050      		subi r30,lo8(-(keymaps))
  77 001e F040      		sbci r31,hi8(-(keymaps))
  78               	.LVL5:
  79               	/* #APP */
  80               	 ;  182 "quantum/keymap_common.c" 1
  81 0020 8591      		lpm r24, Z+
  82 0022 9491      		lpm r25, Z
  83               		
  84               	 ;  0 "" 2
  85               	.LVL6:
  86               	/* #NOAPP */
  87               	.LBE2:
 183:quantum/keymap_common.c **** }
  88               		.loc 1 183 0
  89 0024 0895      		ret
  90               		.cfi_endproc
  91               	.LFE115:
  93               		.section	.text.keymap_function_id_to_action,"ax",@progbits
  94               		.weak	keymap_function_id_to_action
  96               	keymap_function_id_to_action:
  97               	.LFB116:
 184:quantum/keymap_common.c **** 
 185:quantum/keymap_common.c **** // translates function id to action
 186:quantum/keymap_common.c **** __attribute__((weak)) uint16_t keymap_function_id_to_action(uint16_t function_id) {
  98               		.loc 1 186 0
  99               		.cfi_startproc
 100               	.LVL7:
 101               	/* prologue: function */
 102               	/* frame size = 0 */
 103               	/* stack size = 0 */
 104               	.L__stack_usage = 0
 105               	.LBB3:
 187:quantum/keymap_common.c **** // The compiler sees the empty (weak) fn_actions and generates a warning
 188:quantum/keymap_common.c **** // This function should not be called in that case, so the warning is too strict
 189:quantum/keymap_common.c **** // If this function is called however, the keymap should have overridden fn_actions, and then the c
 190:quantum/keymap_common.c **** // is comparing against the wrong array
 191:quantum/keymap_common.c **** #pragma GCC diagnostic push
 192:quantum/keymap_common.c **** #pragma GCC diagnostic ignored "-Warray-bounds"
 193:quantum/keymap_common.c ****     return pgm_read_word(&fn_actions[function_id]);
 106               		.loc 1 193 0
 107 0000 880F      		lsl r24
 108 0002 991F      		rol r25
 109               	.LVL8:
 110 0004 FC01      		movw r30,r24
 111 0006 E050      		subi r30,lo8(-(fn_actions))
 112 0008 F040      		sbci r31,hi8(-(fn_actions))
 113               	.LVL9:
 114               	/* #APP */
 115               	 ;  193 "quantum/keymap_common.c" 1
 116 000a 8591      		lpm r24, Z+
 117 000c 9491      		lpm r25, Z
 118               		
 119               	 ;  0 "" 2
 120               	.LVL10:
 121               	/* #NOAPP */
 122               	.LBE3:
 194:quantum/keymap_common.c **** #pragma GCC diagnostic pop
 195:quantum/keymap_common.c **** }
 123               		.loc 1 195 0
 124 000e 0895      		ret
 125               		.cfi_endproc
 126               	.LFE116:
 128               		.section	.text.action_for_key,"ax",@progbits
 129               	.global	action_for_key
 131               	action_for_key:
 132               	.LFB112:
  44:quantum/keymap_common.c ****     // 16bit keycodes - important
 133               		.loc 1 44 0
 134               		.cfi_startproc
 135               	.LVL11:
 136 0000 CF93      		push r28
 137               	.LCFI0:
 138               		.cfi_def_cfa_offset 3
 139               		.cfi_offset 28, -2
 140 0002 DF93      		push r29
 141               	.LCFI1:
 142               		.cfi_def_cfa_offset 4
 143               		.cfi_offset 29, -3
 144               	/* prologue: function */
 145               	/* frame size = 0 */
 146               	/* stack size = 2 */
 147               	.L__stack_usage = 2
  46:quantum/keymap_common.c **** 
 148               		.loc 1 46 0
 149 0004 0E94 0000 		call keymap_key_to_keycode
 150               	.LVL12:
  49:quantum/keymap_common.c **** 
 151               		.loc 1 49 0
 152 0008 0E94 0000 		call keycode_config
 153               	.LVL13:
 154 000c EC01      		movw r28,r24
 155               	.LVL14:
  58:quantum/keymap_common.c ****         case KC_A ... KC_EXSEL:
 156               		.loc 1 58 0
 157 000e 8115      		cp r24,__zero_reg__
 158 0010 21E5      		ldi r18,81
 159 0012 9207      		cpc r25,r18
 160 0014 00F4      		brsh .L7
 161 0016 8115      		cp r24,__zero_reg__
 162 0018 40E5      		ldi r20,80
 163 001a 9407      		cpc r25,r20
 164 001c 00F0      		brlo .+2
 165 001e 00C0      		rjmp .L8
 166 0020 883E      		cpi r24,-24
 167 0022 9105      		cpc r25,__zero_reg__
 168 0024 00F4      		brsh .L9
 169 0026 803E      		cpi r24,-32
 170 0028 9105      		cpc r25,__zero_reg__
 171 002a 00F0      		brlo .+2
 172 002c 00C0      		rjmp .L10
 173 002e 883A      		cpi r24,-88
 174 0030 9105      		cpc r25,__zero_reg__
 175 0032 00F4      		brsh .L11
 176 0034 853A      		cpi r24,-91
 177 0036 9105      		cpc r25,__zero_reg__
 178 0038 00F0      		brlo .+2
 179 003a 00C0      		rjmp .L12
 180 003c 8130      		cpi r24,1
 181 003e 9105      		cpc r25,__zero_reg__
 182 0040 01F4      		brne .+2
 183 0042 00C0      		rjmp .L10
 184 0044 00F4      		brsh .+2
 185 0046 00C0      		rjmp .L6
 186 0048 0497      		sbiw r24,4
 187 004a 00F0      		brlo .+2
 188 004c 00C0      		rjmp .L10
 189 004e 00C0      		rjmp .L6
 190               	.L11:
 191 0050 8F3B      		cpi r24,-65
 192 0052 9105      		cpc r25,__zero_reg__
 193 0054 00F4      		brsh .+2
 194 0056 00C0      		rjmp .L13
 195 0058 803C      		cpi r24,-64
 196 005a 9105      		cpc r25,__zero_reg__
 197 005c 00F4      		brsh .+2
 198 005e 00C0      		rjmp .L6
  86:quantum/keymap_common.c ****             break;
 199               		.loc 1 86 0
 200 0060 805C      		subi r24,-64
 201 0062 9109      		sbc r25,__zero_reg__
 202               	.LVL15:
 203 0064 00C0      		rjmp .L41
 204               	.LVL16:
 205               	.L9:
  58:quantum/keymap_common.c ****         case KC_A ... KC_EXSEL:
 206               		.loc 1 58 0
 207 0066 C115      		cp r28,__zero_reg__
 208 0068 80E3      		ldi r24,48
 209 006a D807      		cpc r29,r24
 210               	.LVL17:
 211 006c 00F4      		brsh .L15
 212 006e C115      		cp r28,__zero_reg__
 213 0070 20E2      		ldi r18,32
 214 0072 D207      		cpc r29,r18
 215 0074 00F0      		brlo .+2
 216 0076 00C0      		rjmp .L16
 217 0078 C115      		cp r28,__zero_reg__
 218 007a 41E0      		ldi r20,1
 219 007c D407      		cpc r29,r20
 220 007e 00F0      		brlo .+2
 221 0080 00C0      		rjmp .L10
 222 0082 00C0      		rjmp .L6
 223               	.L15:
 224 0084 C115      		cp r28,__zero_reg__
 225 0086 80E4      		ldi r24,64
 226 0088 D807      		cpc r29,r24
 227 008a 00F0      		brlo .+2
 228 008c 00C0      		rjmp .L36
 229 008e 00C0      		rjmp .L43
 230               	.LVL18:
 231               	.L7:
 232 0090 8115      		cp r24,__zero_reg__
 233 0092 26E5      		ldi r18,86
 234 0094 9207      		cpc r25,r18
 235 0096 00F4      		brsh .L19
 236 0098 8115      		cp r24,__zero_reg__
 237 009a 45E5      		ldi r20,85
 238 009c 9407      		cpc r25,r20
 239 009e 00F0      		brlo .+2
 240 00a0 00C0      		rjmp .L20
 241 00a2 C115      		cp r28,__zero_reg__
 242 00a4 83E5      		ldi r24,83
 243 00a6 D807      		cpc r29,r24
 244               	.LVL19:
 245 00a8 00F4      		brsh .L21
 246 00aa C115      		cp r28,__zero_reg__
 247 00ac 22E5      		ldi r18,82
 248 00ae D207      		cpc r29,r18
 249 00b0 00F0      		brlo .+2
 250 00b2 00C0      		rjmp .L44
 251               	.LVL20:
 115:quantum/keymap_common.c ****             break;
 252               		.loc 1 115 0
 253 00b4 DC2F      		mov r29,r28
 254 00b6 CC27      		clr r28
 255               	.LVL21:
 256 00b8 C16F      		ori r28,241
 257 00ba D06A      		ori r29,160
 258               	.LVL22:
 116:quantum/keymap_common.c ****         case QK_DEF_LAYER ... QK_DEF_LAYER_MAX:;
 259               		.loc 1 116 0
 260 00bc 00C0      		rjmp .L10
 261               	.LVL23:
 262               	.L21:
  58:quantum/keymap_common.c ****         case KC_A ... KC_EXSEL:
 263               		.loc 1 58 0
 264 00be C115      		cp r28,__zero_reg__
 265 00c0 44E5      		ldi r20,84
 266 00c2 D407      		cpc r29,r20
 267 00c4 00F0      		brlo .+2
 268 00c6 00C0      		rjmp .L38
 269               	.LVL24:
 125:quantum/keymap_common.c ****             break;
 270               		.loc 1 125 0
 271 00c8 CE01      		movw r24,r28
 272 00ca 8370      		andi r24,3
 273 00cc 9927      		clr r25
 274 00ce 21E0      		ldi r18,lo8(1)
 275 00d0 30E0      		ldi r19,0
 276 00d2 00C0      		rjmp 2f
 277               		1:
 278 00d4 220F      		lsl r18
 279 00d6 331F      		rol r19
 280               		2:
 281 00d8 8A95      		dec r24
 282 00da 02F4      		brpl 1b
 283 00dc 8C2F      		mov r24,r28
 284 00de 8695      		lsr r24
 285 00e0 8695      		lsr r24
 286 00e2 40E2      		ldi r20,lo8(32)
 287 00e4 849F      		mul r24,r20
 288 00e6 C001      		movw r24,r0
 289 00e8 1124      		clr __zero_reg__
 290 00ea E901      		movw r28,r18
 291               	.LVL25:
 292 00ec C82B      		or r28,r24
 293 00ee D92B      		or r29,r25
 294 00f0 DA68      		ori r29,138
 295               	.LVL26:
 126:quantum/keymap_common.c **** #endif
 296               		.loc 1 126 0
 297 00f2 00C0      		rjmp .L10
 298               	.LVL27:
 299               	.L19:
  58:quantum/keymap_common.c ****         case KC_A ... KC_EXSEL:
 300               		.loc 1 58 0
 301 00f4 C115      		cp r28,__zero_reg__
 302 00f6 8AE5      		ldi r24,90
 303 00f8 D807      		cpc r29,r24
 304               	.LVL28:
 305 00fa 00F4      		brsh .L26
 306 00fc C115      		cp r28,__zero_reg__
 307 00fe 29E5      		ldi r18,89
 308 0100 D207      		cpc r29,r18
 309 0102 00F0      		brlo .+2
 310 0104 00C0      		rjmp .L27
 311 0106 C115      		cp r28,__zero_reg__
 312 0108 48E5      		ldi r20,88
 313 010a D407      		cpc r29,r20
 314 010c 00F4      		brsh .+2
 315 010e 00C0      		rjmp .L6
 142:quantum/keymap_common.c ****             break;
 316               		.loc 1 142 0
 317 0110 DC2F      		mov r29,r28
 318 0112 CC27      		clr r28
 319               	.LVL29:
 320 0114 C06F      		ori r28,240
 321 0116 D06A      		ori r29,160
 322               	.LVL30:
 143:quantum/keymap_common.c ****         case QK_LAYER_MOD ... QK_LAYER_MOD_MAX:
 323               		.loc 1 143 0
 324 0118 00C0      		rjmp .L10
 325               	.LVL31:
 326               	.L26:
  58:quantum/keymap_common.c ****         case KC_A ... KC_EXSEL:
 327               		.loc 1 58 0
 328 011a C115      		cp r28,__zero_reg__
 329 011c 8BE5      		ldi r24,91
 330 011e D807      		cpc r29,r24
 331 0120 00F4      		brsh .+2
 332 0122 00C0      		rjmp .L6
 333 0124 C115      		cp r28,__zero_reg__
 334 0126 2CE5      		ldi r18,92
 335 0128 D207      		cpc r29,r18
 336 012a 00F4      		brsh .+2
 337 012c 00C0      		rjmp .L29
 338 012e CE01      		movw r24,r28
 339 0130 9056      		subi r25,96
 340 0132 8115      		cp r24,__zero_reg__
 341 0134 9042      		sbci r25,32
 342 0136 00F0      		brlo .+2
 343 0138 00C0      		rjmp .L6
 152:quantum/keymap_common.c ****             action.code = ACTION_MODS_TAP_KEY(mod, keycode & 0xFF);
 344               		.loc 1 152 0
 345 013a 8D2F      		mov r24,r29
 346 013c 8F71      		andi r24,lo8(31)
 347 013e 0E94 0000 		call mod_config
 348               	.LVL32:
 349 0142 DD27      		clr r29
 350               	.LVL33:
 351 0144 D062      		ori r29,32
 153:quantum/keymap_common.c ****             break;
 352               		.loc 1 153 0
 353 0146 8F71      		andi r24,lo8(31)
 354               	.LVL34:
 355 0148 D82B      		or r29,r24
 356               	.LVL35:
 154:quantum/keymap_common.c **** #endif
 357               		.loc 1 154 0
 358 014a 00C0      		rjmp .L10
 359               	.LVL36:
 360               	.L12:
 361 014c C55A      		subi r28,lo8(-(91))
 362 014e D0E0      		ldi r29,0
 363 0150 C050      		subi r28,lo8(-(CSWTCH.1))
 364 0152 D040      		sbci r29,hi8(-(CSWTCH.1))
 365 0154 C881      		ld r28,Y
 366 0156 D0E0      		ldi r29,0
 367               	.LVL37:
  65:quantum/keymap_common.c ****             break;
 368               		.loc 1 65 0
 369 0158 D064      		ori r29,64
 370               	.LVL38:
  66:quantum/keymap_common.c ****         case KC_AUDIO_MUTE ... KC_BRIGHTNESS_DOWN:
 371               		.loc 1 66 0
 372 015a 00C0      		rjmp .L10
 373               	.LVL39:
 374               	.L13:
 375 015c C85A      		subi r28,lo8(-(88))
 376 015e D0E0      		ldi r29,0
 377 0160 CC0F      		lsl r28
 378 0162 DD1F      		rol r29
 379 0164 C050      		subi r28,lo8(-(CSWTCH.3))
 380 0166 D040      		sbci r29,hi8(-(CSWTCH.3))
 381 0168 0990      		ld __tmp_reg__,Y+
 382 016a D881      		ld r29,Y
 383 016c C02D      		mov r28,__tmp_reg__
 384               	.LVL40:
  68:quantum/keymap_common.c ****             break;
 385               		.loc 1 68 0
 386 016e D464      		ori r29,68
 387               	.LVL41:
  69:quantum/keymap_common.c **** #endif
 388               		.loc 1 69 0
 389 0170 00C0      		rjmp .L10
 390               	.LVL42:
 391               	.L16:
  91:quantum/keymap_common.c ****             break;
 392               		.loc 1 91 0
 393 0172 CE01      		movw r24,r28
 394 0174 9F70      		andi r25,15
 395               	.L41:
 396 0176 0E94 0000 		call keymap_function_id_to_action
 397               	.LVL43:
 398 017a EC01      		movw r28,r24
 399               	.LVL44:
  92:quantum/keymap_common.c **** #endif
 400               		.loc 1 92 0
 401 017c 00C0      		rjmp .L10
 402               	.LVL45:
 403               	.L43:
 404 017e CE01      		movw r24,r28
 405 0180 9927      		clr r25
  96:quantum/keymap_common.c ****                 action.code = ACTION_MACRO_TAP(keycode & 0xFF);
 406               		.loc 1 96 0
 407 0182 D3FF      		sbrs r29,3
 408 0184 00C0      		rjmp .L31
  97:quantum/keymap_common.c ****             else
 409               		.loc 1 97 0
 410 0186 EC01      		movw r28,r24
 411               	.LVL46:
 412 0188 D86C      		ori r29,200
 413               	.LVL47:
 414 018a 00C0      		rjmp .L10
 415               	.LVL48:
 416               	.L31:
  99:quantum/keymap_common.c ****             break;
 417               		.loc 1 99 0
 418 018c EC01      		movw r28,r24
 419               	.LVL49:
 420 018e D06C      		ori r29,192
 421               	.LVL50:
 422 0190 00C0      		rjmp .L10
 423               	.LVL51:
 424               	.L36:
 104:quantum/keymap_common.c ****             break;
 425               		.loc 1 104 0
 426 0192 DF70      		andi r29,15
 427               	.LVL52:
 428 0194 D06A      		ori r29,160
 429               	.LVL53:
 105:quantum/keymap_common.c ****         case QK_TO ... QK_TO_MAX:;
 430               		.loc 1 105 0
 431 0196 00C0      		rjmp .L10
 432               	.LVL54:
 433               	.L8:
 110:quantum/keymap_common.c ****             break;
 434               		.loc 1 110 0
 435 0198 74E0      		ldi r23,4
 436               		1:
 437 019a 9695      		lsr r25
 438 019c 8795      		ror r24
 439 019e 7A95      		dec r23
 440 01a0 01F4      		brne 1b
 441               	.LVL55:
 442 01a2 8370      		andi r24,lo8(3)
 443 01a4 9C2F      		mov r25,r28
 444 01a6 9370      		andi r25,lo8(3)
 445 01a8 21E0      		ldi r18,lo8(1)
 446 01aa 30E0      		ldi r19,0
 447 01ac 00C0      		rjmp 2f
 448               		1:
 449 01ae 220F      		lsl r18
 450 01b0 331F      		rol r19
 451               		2:
 452 01b2 9A95      		dec r25
 453 01b4 02F4      		brpl 1b
 454 01b6 3C68      		ori r19,140
 455 01b8 382B      		or r19,r24
 456 01ba CF70      		andi r28,lo8(15)
 457               	.LVL56:
 458 01bc C695      		lsr r28
 459 01be C695      		lsr r28
 460 01c0 80E2      		ldi r24,lo8(32)
 461 01c2 C89F      		mul r28,r24
 462 01c4 E001      		movw r28,r0
 463 01c6 1124      		clr __zero_reg__
 464 01c8 C22B      		or r28,r18
 465 01ca D32B      		or r29,r19
 466               	.LVL57:
 111:quantum/keymap_common.c ****         case QK_MOMENTARY ... QK_MOMENTARY_MAX:;
 467               		.loc 1 111 0
 468 01cc 00C0      		rjmp .L10
 469               	.LVL58:
 470               	.L44:
 120:quantum/keymap_common.c ****             break;
 471               		.loc 1 120 0
 472 01ce CE01      		movw r24,r28
 473 01d0 8370      		andi r24,3
 474 01d2 9927      		clr r25
 475 01d4 21E0      		ldi r18,lo8(1)
 476 01d6 30E0      		ldi r19,0
 477 01d8 00C0      		rjmp 2f
 478               		1:
 479 01da 220F      		lsl r18
 480 01dc 331F      		rol r19
 481               		2:
 482 01de 8A95      		dec r24
 483 01e0 02F4      		brpl 1b
 484 01e2 8C2F      		mov r24,r28
 485 01e4 8695      		lsr r24
 486 01e6 8695      		lsr r24
 487 01e8 40E2      		ldi r20,lo8(32)
 488 01ea 849F      		mul r24,r20
 489 01ec C001      		movw r24,r0
 490 01ee 1124      		clr __zero_reg__
 491 01f0 E901      		movw r28,r18
 492               	.LVL59:
 493 01f2 C82B      		or r28,r24
 494 01f4 D92B      		or r29,r25
 495 01f6 DC68      		ori r29,140
 496               	.LVL60:
 121:quantum/keymap_common.c ****         case QK_TOGGLE_LAYER ... QK_TOGGLE_LAYER_MAX:;
 497               		.loc 1 121 0
 498 01f8 00C0      		rjmp .L10
 499               	.LVL61:
 500               	.L38:
 132:quantum/keymap_common.c ****             break;
 501               		.loc 1 132 0
 502 01fa DC2F      		mov r29,r28
 503 01fc CC27      		clr r28
 504               	.LVL62:
 505 01fe C46F      		ori r28,244
 506 0200 D06A      		ori r29,160
 507               	.LVL63:
 133:quantum/keymap_common.c ****         case QK_ONE_SHOT_MOD ... QK_ONE_SHOT_MOD_MAX:;
 508               		.loc 1 133 0
 509 0202 00C0      		rjmp .L10
 510               	.LVL64:
 511               	.L20:
 136:quantum/keymap_common.c ****             action.code = ACTION_MODS_ONESHOT(mod);
 512               		.loc 1 136 0
 513 0204 0E94 0000 		call mod_config
 514               	.LVL65:
 137:quantum/keymap_common.c ****             break;
 515               		.loc 1 137 0
 516 0208 8F71      		andi r24,lo8(31)
 517               	.LVL66:
 518 020a C82F      		mov r28,r24
 519               	.LVL67:
 520 020c D0E0      		ldi r29,0
 521 020e DC2F      		mov r29,r28
 522 0210 CC27      		clr r28
 523 0212 D062      		ori r29,32
 524               	.LVL68:
 138:quantum/keymap_common.c **** #endif
 525               		.loc 1 138 0
 526 0214 00C0      		rjmp .L10
 527               	.LVL69:
 528               	.L27:
 145:quantum/keymap_common.c ****             action_layer = (keycode >> 4) & 0xF;
 529               		.loc 1 145 0
 530 0216 8C2F      		mov r24,r28
 531 0218 8F70      		andi r24,lo8(15)
 532 021a 0E94 0000 		call mod_config
 533               	.LVL70:
 147:quantum/keymap_common.c ****             break;
 534               		.loc 1 147 0
 535 021e 94E0      		ldi r25,4
 536               		1:
 537 0220 D695      		lsr r29
 538 0222 C795      		ror r28
 539 0224 9A95      		dec r25
 540 0226 01F4      		brne 1b
 541               	.LVL71:
 542 0228 CF70      		andi r28,lo8(15)
 543 022a D0E0      		ldi r29,0
 544 022c DC2F      		mov r29,r28
 545 022e CC27      		clr r28
 546 0230 D069      		ori r29,144
 547 0232 C82B      		or r28,r24
 548               	.LVL72:
 148:quantum/keymap_common.c **** #endif
 549               		.loc 1 148 0
 550 0234 00C0      		rjmp .L10
 551               	.LVL73:
 552               	.L29:
 158:quantum/keymap_common.c ****             break;
 553               		.loc 1 158 0
 554 0236 DD27      		clr r29
 555               	.LVL74:
 556 0238 D066      		ori r29,96
 557               	.LVL75:
 159:quantum/keymap_common.c **** #endif
 558               		.loc 1 159 0
 559 023a 00C0      		rjmp .L10
 560               	.LVL76:
 561               	.L6:
 163:quantum/keymap_common.c ****             break;
 562               		.loc 1 163 0
 563 023c C0E0      		ldi r28,0
 564 023e D0E0      		ldi r29,0
 565               	.LVL77:
 566               	.L10:
 167:quantum/keymap_common.c **** 
 567               		.loc 1 167 0
 568 0240 CE01      		movw r24,r28
 569               	/* epilogue start */
 570 0242 DF91      		pop r29
 571 0244 CF91      		pop r28
 572 0246 0895      		ret
 573               		.cfi_endproc
 574               	.LFE112:
 576               		.section	.rodata.CSWTCH.3,"a",@progbits
 579               	CSWTCH.3:
 580 0000 E200      		.word	226
 581 0002 E900      		.word	233
 582 0004 EA00      		.word	234
 583 0006 B500      		.word	181
 584 0008 B600      		.word	182
 585 000a B700      		.word	183
 586 000c CD00      		.word	205
 587 000e 8301      		.word	387
 588 0010 CC00      		.word	204
 589 0012 8A01      		.word	394
 590 0014 9201      		.word	402
 591 0016 9401      		.word	404
 592 0018 2102      		.word	545
 593 001a 2302      		.word	547
 594 001c 2402      		.word	548
 595 001e 2502      		.word	549
 596 0020 2602      		.word	550
 597 0022 2702      		.word	551
 598 0024 2A02      		.word	554
 599 0026 B300      		.word	179
 600 0028 B400      		.word	180
 601 002a 6F00      		.word	111
 602 002c 7000      		.word	112
 603               		.section	.rodata.CSWTCH.1,"a",@progbits
 606               	CSWTCH.1:
 607 0000 81        		.byte	-127
 608 0001 82        		.byte	-126
 609 0002 83        		.byte	-125
 610               		.weak	fn_actions
 611               		.section	.progmem.data.fn_actions,"a",@progbits
 614               	fn_actions:
 615               		.text
 616               	.Letext0:
 617               		.file 2 "/usr/lib/avr/include/stdint.h"
 618               		.file 3 "tmk_core/common/keyboard.h"
 619               		.file 4 "tmk_core/common/keycode.h"
 620               		.file 5 "tmk_core/common/action_code.h"
 621               		.file 6 "tmk_core/common/action_macro.h"
 622               		.file 7 "tmk_core/common/action.h"
 623               		.file 8 "tmk_core/common/report.h"
 624               		.file 9 "quantum/quantum_keycodes.h"
 625               		.file 10 "lib/lufa/LUFA/Drivers/USB/Core/USBTask.h"
 626               		.file 11 "quantum/keymap.h"
 627               		.file 12 "tmk_core/common/action_util.h"
 628               		.file 13 "quantum/keycode_config.h"
DEFINED SYMBOLS
                            *ABS*:0000000000000000 keymap_common.c
     /tmp/cc28YxCy.s:2      *ABS*:000000000000003e __SP_H__
     /tmp/cc28YxCy.s:3      *ABS*:000000000000003d __SP_L__
     /tmp/cc28YxCy.s:4      *ABS*:000000000000003f __SREG__
     /tmp/cc28YxCy.s:5      *ABS*:0000000000000000 __tmp_reg__
     /tmp/cc28YxCy.s:6      *ABS*:0000000000000001 __zero_reg__
     /tmp/cc28YxCy.s:13     .text.action_get_macro:0000000000000000 action_get_macro
     /tmp/cc28YxCy.s:34     .text.action_function:0000000000000000 action_function
     /tmp/cc28YxCy.s:50     .text.keymap_key_to_keycode:0000000000000000 keymap_key_to_keycode
     /tmp/cc28YxCy.s:96     .text.keymap_function_id_to_action:0000000000000000 keymap_function_id_to_action
     /tmp/cc28YxCy.s:614    .progmem.data.fn_actions:0000000000000000 fn_actions
     /tmp/cc28YxCy.s:131    .text.action_for_key:0000000000000000 action_for_key
     /tmp/cc28YxCy.s:606    .rodata.CSWTCH.1:0000000000000000 CSWTCH.1
     /tmp/cc28YxCy.s:579    .rodata.CSWTCH.3:0000000000000000 CSWTCH.3

UNDEFINED SYMBOLS
keymaps
keycode_config
mod_config
__do_copy_data
