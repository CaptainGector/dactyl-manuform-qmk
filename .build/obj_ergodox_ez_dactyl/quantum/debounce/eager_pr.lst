   1               		.file	"eager_pr.c"
   2               	__SP_H__ = 0x3e
   3               	__SP_L__ = 0x3d
   4               	__SREG__ = 0x3f
   5               	__tmp_reg__ = 0
   6               	__zero_reg__ = 1
   7               		.text
   8               	.Ltext0:
   9               		.cfi_sections	.debug_frame
  10               		.section	.text.debounce_init,"ax",@progbits
  11               	.global	debounce_init
  13               	debounce_init:
  14               	.LFB113:
  15               		.file 1 "quantum/debounce/eager_pr.c"
   1:quantum/debounce/eager_pr.c **** /*
   2:quantum/debounce/eager_pr.c **** Copyright 2019 Alex Ong<the.onga@gmail.com>
   3:quantum/debounce/eager_pr.c **** This program is free software: you can redistribute it and/or modify
   4:quantum/debounce/eager_pr.c **** it under the terms of the GNU General Public License as published by
   5:quantum/debounce/eager_pr.c **** the Free Software Foundation, either version 2 of the License, or
   6:quantum/debounce/eager_pr.c **** (at your option) any later version.
   7:quantum/debounce/eager_pr.c **** This program is distributed in the hope that it will be useful,
   8:quantum/debounce/eager_pr.c **** but WITHOUT ANY WARRANTY; without even the implied warranty of
   9:quantum/debounce/eager_pr.c **** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  10:quantum/debounce/eager_pr.c **** GNU General Public License for more details.
  11:quantum/debounce/eager_pr.c **** You should have received a copy of the GNU General Public License
  12:quantum/debounce/eager_pr.c **** along with this program.  If not, see <http://www.gnu.org/licenses/>.
  13:quantum/debounce/eager_pr.c **** */
  14:quantum/debounce/eager_pr.c **** 
  15:quantum/debounce/eager_pr.c **** /*
  16:quantum/debounce/eager_pr.c **** Basic per-row algorithm. Uses an 8-bit counter per row.
  17:quantum/debounce/eager_pr.c **** After pressing a key, it immediately changes state, and sets a counter.
  18:quantum/debounce/eager_pr.c **** No further inputs are accepted until DEBOUNCE milliseconds have occurred.
  19:quantum/debounce/eager_pr.c **** */
  20:quantum/debounce/eager_pr.c **** 
  21:quantum/debounce/eager_pr.c **** #include "matrix.h"
  22:quantum/debounce/eager_pr.c **** #include "timer.h"
  23:quantum/debounce/eager_pr.c **** #include "quantum.h"
  24:quantum/debounce/eager_pr.c **** #include <stdlib.h>
  25:quantum/debounce/eager_pr.c **** 
  26:quantum/debounce/eager_pr.c **** #ifndef DEBOUNCE
  27:quantum/debounce/eager_pr.c **** #    define DEBOUNCE 5
  28:quantum/debounce/eager_pr.c **** #endif
  29:quantum/debounce/eager_pr.c **** 
  30:quantum/debounce/eager_pr.c **** #define debounce_counter_t uint8_t
  31:quantum/debounce/eager_pr.c **** static bool matrix_need_update;
  32:quantum/debounce/eager_pr.c **** 
  33:quantum/debounce/eager_pr.c **** static debounce_counter_t *debounce_counters;
  34:quantum/debounce/eager_pr.c **** static bool                counters_need_update;
  35:quantum/debounce/eager_pr.c **** 
  36:quantum/debounce/eager_pr.c **** #define DEBOUNCE_ELAPSED 251
  37:quantum/debounce/eager_pr.c **** #define MAX_DEBOUNCE (DEBOUNCE_ELAPSED - 1)
  38:quantum/debounce/eager_pr.c **** 
  39:quantum/debounce/eager_pr.c **** static uint8_t wrapping_timer_read(void) {
  40:quantum/debounce/eager_pr.c ****     static uint16_t time        = 0;
  41:quantum/debounce/eager_pr.c ****     static uint8_t  last_result = 0;
  42:quantum/debounce/eager_pr.c ****     uint16_t        new_time    = timer_read();
  43:quantum/debounce/eager_pr.c ****     uint16_t        diff        = new_time - time;
  44:quantum/debounce/eager_pr.c ****     time                        = new_time;
  45:quantum/debounce/eager_pr.c ****     last_result                 = (last_result + diff) % (MAX_DEBOUNCE + 1);
  46:quantum/debounce/eager_pr.c ****     return last_result;
  47:quantum/debounce/eager_pr.c **** }
  48:quantum/debounce/eager_pr.c **** 
  49:quantum/debounce/eager_pr.c **** void update_debounce_counters(uint8_t num_rows, uint8_t current_time);
  50:quantum/debounce/eager_pr.c **** void transfer_matrix_values(matrix_row_t raw[], matrix_row_t cooked[], uint8_t num_rows, uint8_t cu
  51:quantum/debounce/eager_pr.c **** 
  52:quantum/debounce/eager_pr.c **** // we use num_rows rather than MATRIX_ROWS to support split keyboards
  53:quantum/debounce/eager_pr.c **** void debounce_init(uint8_t num_rows) {
  16               		.loc 1 53 0
  17               		.cfi_startproc
  18               	.LVL0:
  19 0000 CF93      		push r28
  20               	.LCFI0:
  21               		.cfi_def_cfa_offset 3
  22               		.cfi_offset 28, -2
  23               	/* prologue: function */
  24               	/* frame size = 0 */
  25               	/* stack size = 1 */
  26               	.L__stack_usage = 1
  27 0002 C82F      		mov r28,r24
  54:quantum/debounce/eager_pr.c ****     debounce_counters = (debounce_counter_t *)malloc(num_rows * sizeof(debounce_counter_t));
  28               		.loc 1 54 0
  29 0004 90E0      		ldi r25,0
  30 0006 0E94 0000 		call malloc
  31               	.LVL1:
  32 000a 8093 0000 		sts debounce_counters,r24
  33 000e 9093 0000 		sts debounce_counters+1,r25
  34               	.LVL2:
  35 0012 282F      		mov r18,r24
  36               	.LBB2:
  55:quantum/debounce/eager_pr.c ****     for (uint8_t r = 0; r < num_rows; r++) {
  37               		.loc 1 55 0
  38 0014 FC01      		movw r30,r24
  56:quantum/debounce/eager_pr.c ****         debounce_counters[r] = DEBOUNCE_ELAPSED;
  39               		.loc 1 56 0
  40 0016 9BEF      		ldi r25,lo8(-5)
  41               	.LVL3:
  42               	.L2:
  55:quantum/debounce/eager_pr.c ****     for (uint8_t r = 0; r < num_rows; r++) {
  43               		.loc 1 55 0 discriminator 1
  44 0018 8E2F      		mov r24,r30
  45 001a 821B      		sub r24,r18
  46 001c 8C17      		cp r24,r28
  47 001e 00F4      		brsh .L5
  48               		.loc 1 56 0 discriminator 3
  49 0020 9193      		st Z+,r25
  50               	.LVL4:
  51 0022 00C0      		rjmp .L2
  52               	.L5:
  53               	/* epilogue start */
  54               	.LBE2:
  57:quantum/debounce/eager_pr.c ****     }
  58:quantum/debounce/eager_pr.c **** }
  55               		.loc 1 58 0
  56 0024 CF91      		pop r28
  57               	.LVL5:
  58 0026 0895      		ret
  59               		.cfi_endproc
  60               	.LFE113:
  62               		.section	.text.update_debounce_counters,"ax",@progbits
  63               	.global	update_debounce_counters
  65               	update_debounce_counters:
  66               	.LFB115:
  59:quantum/debounce/eager_pr.c **** 
  60:quantum/debounce/eager_pr.c **** void debounce(matrix_row_t raw[], matrix_row_t cooked[], uint8_t num_rows, bool changed) {
  61:quantum/debounce/eager_pr.c ****     uint8_t current_time  = wrapping_timer_read();
  62:quantum/debounce/eager_pr.c ****     bool    needed_update = counters_need_update;
  63:quantum/debounce/eager_pr.c ****     if (counters_need_update) {
  64:quantum/debounce/eager_pr.c ****         update_debounce_counters(num_rows, current_time);
  65:quantum/debounce/eager_pr.c ****     }
  66:quantum/debounce/eager_pr.c **** 
  67:quantum/debounce/eager_pr.c ****     if (changed || (needed_update && !counters_need_update) || matrix_need_update) {
  68:quantum/debounce/eager_pr.c ****         transfer_matrix_values(raw, cooked, num_rows, current_time);
  69:quantum/debounce/eager_pr.c ****     }
  70:quantum/debounce/eager_pr.c **** }
  71:quantum/debounce/eager_pr.c **** 
  72:quantum/debounce/eager_pr.c **** // If the current time is > debounce counter, set the counter to enable input.
  73:quantum/debounce/eager_pr.c **** void update_debounce_counters(uint8_t num_rows, uint8_t current_time) {
  67               		.loc 1 73 0
  68               		.cfi_startproc
  69               	.LVL6:
  70 0000 0F93      		push r16
  71               	.LCFI1:
  72               		.cfi_def_cfa_offset 3
  73               		.cfi_offset 16, -2
  74 0002 1F93      		push r17
  75               	.LCFI2:
  76               		.cfi_def_cfa_offset 4
  77               		.cfi_offset 17, -3
  78 0004 CF93      		push r28
  79               	.LCFI3:
  80               		.cfi_def_cfa_offset 5
  81               		.cfi_offset 28, -4
  82 0006 DF93      		push r29
  83               	.LCFI4:
  84               		.cfi_def_cfa_offset 6
  85               		.cfi_offset 29, -5
  86               	/* prologue: function */
  87               	/* frame size = 0 */
  88               	/* stack size = 4 */
  89               	.L__stack_usage = 4
  90 0008 A62F      		mov r26,r22
  74:quantum/debounce/eager_pr.c ****     counters_need_update                 = false;
  91               		.loc 1 74 0
  92 000a 1092 0000 		sts counters_need_update,__zero_reg__
  75:quantum/debounce/eager_pr.c ****     debounce_counter_t *debounce_pointer = debounce_counters;
  93               		.loc 1 75 0
  94 000e 3091 0000 		lds r19,debounce_counters
  95               	.LVL7:
  96 0012 E32F      		mov r30,r19
  97 0014 F091 0000 		lds r31,debounce_counters+1
  98               	.LBB3:
  76:quantum/debounce/eager_pr.c ****     for (uint8_t row = 0; row < num_rows; row++) {
  99               		.loc 1 76 0
 100 0018 20E0      		ldi r18,0
 101 001a C62F      		mov r28,r22
 102 001c D0E0      		ldi r29,0
  77:quantum/debounce/eager_pr.c ****         if (*debounce_pointer != DEBOUNCE_ELAPSED) {
  78:quantum/debounce/eager_pr.c ****             if (TIMER_DIFF(current_time, *debounce_pointer, MAX_DEBOUNCE) >= DEBOUNCE) {
  79:quantum/debounce/eager_pr.c ****                 *debounce_pointer = DEBOUNCE_ELAPSED;
 103               		.loc 1 79 0
 104 001e BBEF      		ldi r27,lo8(-5)
  78:quantum/debounce/eager_pr.c ****                 *debounce_pointer = DEBOUNCE_ELAPSED;
 105               		.loc 1 78 0
 106 0020 0BEF      		ldi r16,lo8(-5)
 107 0022 10E0      		ldi r17,0
 108               	.LVL8:
 109               	.L7:
  76:quantum/debounce/eager_pr.c ****     for (uint8_t row = 0; row < num_rows; row++) {
 110               		.loc 1 76 0 discriminator 1
 111 0024 9E2F      		mov r25,r30
 112 0026 931B      		sub r25,r19
 113 0028 9817      		cp r25,r24
 114 002a 00F4      		brsh .L16
  77:quantum/debounce/eager_pr.c ****         if (*debounce_pointer != DEBOUNCE_ELAPSED) {
 115               		.loc 1 77 0
 116 002c 9081      		ld r25,Z
 117 002e 9B3F      		cpi r25,lo8(-5)
 118 0030 01F0      		breq .L8
 119 0032 492F      		mov r20,r25
 120 0034 50E0      		ldi r21,0
  78:quantum/debounce/eager_pr.c ****                 *debounce_pointer = DEBOUNCE_ELAPSED;
 121               		.loc 1 78 0
 122 0036 A917      		cp r26,r25
 123 0038 00F0      		brlo .L9
  78:quantum/debounce/eager_pr.c ****                 *debounce_pointer = DEBOUNCE_ELAPSED;
 124               		.loc 1 78 0 is_stmt 0 discriminator 1
 125 003a BE01      		movw r22,r28
 126 003c 641B      		sub r22,r20
 127 003e 750B      		sbc r23,r21
 128 0040 AB01      		movw r20,r22
 129 0042 00C0      		rjmp .L10
 130               	.L9:
  78:quantum/debounce/eager_pr.c ****                 *debounce_pointer = DEBOUNCE_ELAPSED;
 131               		.loc 1 78 0 discriminator 2
 132 0044 B801      		movw r22,r16
 133 0046 641B      		sub r22,r20
 134 0048 750B      		sbc r23,r21
 135 004a AB01      		movw r20,r22
 136 004c 4C0F      		add r20,r28
 137 004e 5D1F      		adc r21,r29
 138               	.L10:
  78:quantum/debounce/eager_pr.c ****                 *debounce_pointer = DEBOUNCE_ELAPSED;
 139               		.loc 1 78 0 discriminator 4
 140 0050 052E      		mov __tmp_reg__,r21
 141 0052 000C      		lsl r0
 142 0054 660B      		sbc r22,r22
 143 0056 770B      		sbc r23,r23
 144 0058 4E31      		cpi r20,30
 145 005a 5105      		cpc r21,__zero_reg__
 146 005c 6105      		cpc r22,__zero_reg__
 147 005e 7105      		cpc r23,__zero_reg__
 148 0060 00F0      		brlo .L12
 149               		.loc 1 79 0 is_stmt 1
 150 0062 B083      		st Z,r27
 151 0064 00C0      		rjmp .L8
 152               	.L12:
  80:quantum/debounce/eager_pr.c ****             } else {
  81:quantum/debounce/eager_pr.c ****                 counters_need_update = true;
 153               		.loc 1 81 0
 154 0066 21E0      		ldi r18,lo8(1)
 155               	.L8:
  82:quantum/debounce/eager_pr.c ****             }
  83:quantum/debounce/eager_pr.c ****         }
  84:quantum/debounce/eager_pr.c ****         debounce_pointer++;
 156               		.loc 1 84 0 discriminator 2
 157 0068 3196      		adiw r30,1
 158               	.LVL9:
 159 006a 00C0      		rjmp .L7
 160               	.L16:
 161 006c 2093 0000 		sts counters_need_update,r18
 162               	/* epilogue start */
 163               	.LBE3:
  85:quantum/debounce/eager_pr.c ****     }
  86:quantum/debounce/eager_pr.c **** }
 164               		.loc 1 86 0
 165 0070 DF91      		pop r29
 166 0072 CF91      		pop r28
 167 0074 1F91      		pop r17
 168 0076 0F91      		pop r16
 169 0078 0895      		ret
 170               		.cfi_endproc
 171               	.LFE115:
 173               		.section	.text.transfer_matrix_values,"ax",@progbits
 174               	.global	transfer_matrix_values
 176               	transfer_matrix_values:
 177               	.LFB116:
  87:quantum/debounce/eager_pr.c **** 
  88:quantum/debounce/eager_pr.c **** // upload from raw_matrix to final matrix;
  89:quantum/debounce/eager_pr.c **** void transfer_matrix_values(matrix_row_t raw[], matrix_row_t cooked[], uint8_t num_rows, uint8_t cu
 178               		.loc 1 89 0
 179               		.cfi_startproc
 180               	.LVL10:
 181 0000 CF93      		push r28
 182               	.LCFI5:
 183               		.cfi_def_cfa_offset 3
 184               		.cfi_offset 28, -2
 185 0002 DF93      		push r29
 186               	.LCFI6:
 187               		.cfi_def_cfa_offset 4
 188               		.cfi_offset 29, -3
 189               	/* prologue: function */
 190               	/* frame size = 0 */
 191               	/* stack size = 2 */
 192               	.L__stack_usage = 2
  90:quantum/debounce/eager_pr.c ****     matrix_need_update                   = false;
 193               		.loc 1 90 0
 194 0004 1092 0000 		sts matrix_need_update,__zero_reg__
  91:quantum/debounce/eager_pr.c ****     debounce_counter_t *debounce_pointer = debounce_counters;
 195               		.loc 1 91 0
 196 0008 5091 0000 		lds r21,debounce_counters
 197               	.LVL11:
 198 000c 3091 0000 		lds r19,counters_need_update
 199 0010 DB01      		movw r26,r22
 200 0012 EC01      		movw r28,r24
 201 0014 E52F      		mov r30,r21
 202 0016 F091 0000 		lds r31,debounce_counters+1
 203               	.LBB4:
  92:quantum/debounce/eager_pr.c ****     for (uint8_t row = 0; row < num_rows; row++) {
 204               		.loc 1 92 0
 205 001a 90E0      		ldi r25,0
 206               	.LVL12:
 207               	.L18:
 208               		.loc 1 92 0 is_stmt 0 discriminator 1
 209 001c 8E2F      		mov r24,r30
 210 001e 851B      		sub r24,r21
 211 0020 8417      		cp r24,r20
 212 0022 00F4      		brsh .L22
 213               	.LVL13:
 214               	.LBB5:
  93:quantum/debounce/eager_pr.c ****         matrix_row_t existing_row = cooked[row];
  94:quantum/debounce/eager_pr.c ****         matrix_row_t raw_row      = raw[row];
 215               		.loc 1 94 0 is_stmt 1
 216 0024 8991      		ld r24,Y+
 217               	.LVL14:
  95:quantum/debounce/eager_pr.c **** 
  96:quantum/debounce/eager_pr.c ****         // determine new value basd on debounce pointer + raw value
  97:quantum/debounce/eager_pr.c ****         if (existing_row != raw_row) {
 218               		.loc 1 97 0
 219 0026 6C91      		ld r22,X
 220 0028 6817      		cp r22,r24
 221 002a 01F0      		breq .L19
  98:quantum/debounce/eager_pr.c ****             if (*debounce_pointer == DEBOUNCE_ELAPSED) {
 222               		.loc 1 98 0
 223 002c 6081      		ld r22,Z
 224 002e 6B3F      		cpi r22,lo8(-5)
 225 0030 01F4      		brne .L21
  99:quantum/debounce/eager_pr.c ****                 *debounce_pointer    = current_time;
 226               		.loc 1 99 0
 227 0032 2083      		st Z,r18
 228               	.LVL15:
 100:quantum/debounce/eager_pr.c ****                 cooked[row]          = raw_row;
 229               		.loc 1 100 0
 230 0034 8C93      		st X,r24
 101:quantum/debounce/eager_pr.c ****                 counters_need_update = true;
 231               		.loc 1 101 0
 232 0036 31E0      		ldi r19,lo8(1)
 233 0038 00C0      		rjmp .L19
 234               	.LVL16:
 235               	.L21:
 102:quantum/debounce/eager_pr.c ****             } else {
 103:quantum/debounce/eager_pr.c ****                 matrix_need_update = true;
 236               		.loc 1 103 0
 237 003a 91E0      		ldi r25,lo8(1)
 238               	.LVL17:
 239               	.L19:
 104:quantum/debounce/eager_pr.c ****             }
 105:quantum/debounce/eager_pr.c ****         }
 106:quantum/debounce/eager_pr.c ****         debounce_pointer++;
 240               		.loc 1 106 0 discriminator 2
 241 003c 3196      		adiw r30,1
 242               	.LVL18:
 243 003e 1196      		adiw r26,1
 244 0040 00C0      		rjmp .L18
 245               	.LVL19:
 246               	.L22:
 247 0042 3093 0000 		sts counters_need_update,r19
 248 0046 9093 0000 		sts matrix_need_update,r25
 249               	/* epilogue start */
 250               	.LBE5:
 251               	.LBE4:
 107:quantum/debounce/eager_pr.c ****     }
 108:quantum/debounce/eager_pr.c **** }
 252               		.loc 1 108 0
 253 004a DF91      		pop r29
 254 004c CF91      		pop r28
 255 004e 0895      		ret
 256               		.cfi_endproc
 257               	.LFE116:
 259               		.section	.text.debounce,"ax",@progbits
 260               	.global	debounce
 262               	debounce:
 263               	.LFB114:
  60:quantum/debounce/eager_pr.c ****     uint8_t current_time  = wrapping_timer_read();
 264               		.loc 1 60 0
 265               		.cfi_startproc
 266               	.LVL20:
 267 0000 DF92      		push r13
 268               	.LCFI7:
 269               		.cfi_def_cfa_offset 3
 270               		.cfi_offset 13, -2
 271 0002 EF92      		push r14
 272               	.LCFI8:
 273               		.cfi_def_cfa_offset 4
 274               		.cfi_offset 14, -3
 275 0004 FF92      		push r15
 276               	.LCFI9:
 277               		.cfi_def_cfa_offset 5
 278               		.cfi_offset 15, -4
 279 0006 0F93      		push r16
 280               	.LCFI10:
 281               		.cfi_def_cfa_offset 6
 282               		.cfi_offset 16, -5
 283 0008 1F93      		push r17
 284               	.LCFI11:
 285               		.cfi_def_cfa_offset 7
 286               		.cfi_offset 17, -6
 287 000a CF93      		push r28
 288               	.LCFI12:
 289               		.cfi_def_cfa_offset 8
 290               		.cfi_offset 28, -7
 291 000c DF93      		push r29
 292               	.LCFI13:
 293               		.cfi_def_cfa_offset 9
 294               		.cfi_offset 29, -8
 295               	/* prologue: function */
 296               	/* frame size = 0 */
 297               	/* stack size = 7 */
 298               	.L__stack_usage = 7
 299 000e EC01      		movw r28,r24
 300 0010 8B01      		movw r16,r22
 301 0012 E42E      		mov r14,r20
 302 0014 D22E      		mov r13,r18
 303               	.LBB8:
 304               	.LBB9:
  42:quantum/debounce/eager_pr.c ****     uint16_t        diff        = new_time - time;
 305               		.loc 1 42 0
 306 0016 0E94 0000 		call timer_read
 307               	.LVL21:
 308 001a AC01      		movw r20,r24
 309               	.LVL22:
  43:quantum/debounce/eager_pr.c ****     time                        = new_time;
 310               		.loc 1 43 0
 311 001c 6091 0000 		lds r22,time.6413
 312 0020 7091 0000 		lds r23,time.6413+1
 313               	.LVL23:
  44:quantum/debounce/eager_pr.c ****     last_result                 = (last_result + diff) % (MAX_DEBOUNCE + 1);
 314               		.loc 1 44 0
 315 0024 9093 0000 		sts time.6413+1,r25
 316 0028 8093 0000 		sts time.6413,r24
  45:quantum/debounce/eager_pr.c ****     return last_result;
 317               		.loc 1 45 0
 318 002c 9091 0000 		lds r25,last_result.6414
 319 0030 292F      		mov r18,r25
 320 0032 30E0      		ldi r19,0
 321 0034 261B      		sub r18,r22
 322 0036 370B      		sbc r19,r23
 323 0038 C901      		movw r24,r18
 324 003a 840F      		add r24,r20
 325 003c 951F      		adc r25,r21
 326 003e 6BEF      		ldi r22,lo8(-5)
 327 0040 70E0      		ldi r23,0
 328               	.LVL24:
 329 0042 0E94 0000 		call __udivmodhi4
 330 0046 F82E      		mov r15,r24
 331 0048 8093 0000 		sts last_result.6414,r24
 332               	.LVL25:
 333               	.LBE9:
 334               	.LBE8:
  63:quantum/debounce/eager_pr.c ****         update_debounce_counters(num_rows, current_time);
 335               		.loc 1 63 0
 336 004c 8091 0000 		lds r24,counters_need_update
 337 0050 8823      		tst r24
 338 0052 01F0      		breq .L24
  64:quantum/debounce/eager_pr.c ****     }
 339               		.loc 1 64 0
 340 0054 6F2D      		mov r22,r15
 341 0056 8E2D      		mov r24,r14
 342 0058 0E94 0000 		call update_debounce_counters
 343               	.LVL26:
  67:quantum/debounce/eager_pr.c ****         transfer_matrix_values(raw, cooked, num_rows, current_time);
 344               		.loc 1 67 0
 345 005c DD20      		tst r13
 346 005e 01F0      		breq .L25
 347               	.L26:
  68:quantum/debounce/eager_pr.c ****     }
 348               		.loc 1 68 0
 349 0060 2F2D      		mov r18,r15
 350 0062 4E2D      		mov r20,r14
 351 0064 B801      		movw r22,r16
 352 0066 CE01      		movw r24,r28
 353               	/* epilogue start */
  70:quantum/debounce/eager_pr.c **** 
 354               		.loc 1 70 0
 355 0068 DF91      		pop r29
 356 006a CF91      		pop r28
 357               	.LVL27:
 358 006c 1F91      		pop r17
 359 006e 0F91      		pop r16
 360               	.LVL28:
 361 0070 FF90      		pop r15
 362 0072 EF90      		pop r14
 363               	.LVL29:
 364 0074 DF90      		pop r13
 365               	.LVL30:
  68:quantum/debounce/eager_pr.c ****     }
 366               		.loc 1 68 0
 367 0076 0C94 0000 		jmp transfer_matrix_values
 368               	.LVL31:
 369               	.L25:
  67:quantum/debounce/eager_pr.c ****         transfer_matrix_values(raw, cooked, num_rows, current_time);
 370               		.loc 1 67 0 discriminator 2
 371 007a 8091 0000 		lds r24,counters_need_update
 372 007e 8823      		tst r24
 373 0080 01F0      		breq .L26
 374               	.L27:
  67:quantum/debounce/eager_pr.c ****         transfer_matrix_values(raw, cooked, num_rows, current_time);
 375               		.loc 1 67 0 is_stmt 0
 376 0082 8091 0000 		lds r24,matrix_need_update
 377 0086 8111      		cpse r24,__zero_reg__
 378 0088 00C0      		rjmp .L26
 379               	/* epilogue start */
  70:quantum/debounce/eager_pr.c **** 
 380               		.loc 1 70 0 is_stmt 1
 381 008a DF91      		pop r29
 382 008c CF91      		pop r28
 383               	.LVL32:
 384 008e 1F91      		pop r17
 385 0090 0F91      		pop r16
 386               	.LVL33:
 387 0092 FF90      		pop r15
 388 0094 EF90      		pop r14
 389               	.LVL34:
 390 0096 DF90      		pop r13
 391               	.LVL35:
 392 0098 0895      		ret
 393               	.LVL36:
 394               	.L24:
  67:quantum/debounce/eager_pr.c ****         transfer_matrix_values(raw, cooked, num_rows, current_time);
 395               		.loc 1 67 0
 396 009a D110      		cpse r13,__zero_reg__
 397 009c 00C0      		rjmp .L26
 398 009e 00C0      		rjmp .L27
 399               		.cfi_endproc
 400               	.LFE114:
 402               		.section	.text.debounce_active,"ax",@progbits
 403               	.global	debounce_active
 405               	debounce_active:
 406               	.LFB117:
 109:quantum/debounce/eager_pr.c **** 
 110:quantum/debounce/eager_pr.c **** bool debounce_active(void) { return true; }
 407               		.loc 1 110 0
 408               		.cfi_startproc
 409               	/* prologue: function */
 410               	/* frame size = 0 */
 411               	/* stack size = 0 */
 412               	.L__stack_usage = 0
 413               		.loc 1 110 0
 414 0000 81E0      		ldi r24,lo8(1)
 415 0002 0895      		ret
 416               		.cfi_endproc
 417               	.LFE117:
 419               		.section	.bss.last_result.6414,"aw",@nobits
 422               	last_result.6414:
 423 0000 00        		.zero	1
 424               		.section	.bss.time.6413,"aw",@nobits
 427               	time.6413:
 428 0000 0000      		.zero	2
 429               		.section	.bss.counters_need_update,"aw",@nobits
 432               	counters_need_update:
 433 0000 00        		.zero	1
 434               		.section	.bss.debounce_counters,"aw",@nobits
 437               	debounce_counters:
 438 0000 0000      		.zero	2
 439               		.section	.bss.matrix_need_update,"aw",@nobits
 442               	matrix_need_update:
 443 0000 00        		.zero	1
 444               		.text
 445               	.Letext0:
 446               		.file 2 "/usr/lib/avr/include/stdint.h"
 447               		.file 3 "tmk_core/common/matrix.h"
 448               		.file 4 "tmk_core/common/report.h"
 449               		.file 5 "lib/lufa/LUFA/Drivers/USB/Core/USBTask.h"
 450               		.file 6 "tmk_core/common/action_util.h"
 451               		.file 7 "/usr/lib/avr/include/stdlib.h"
 452               		.file 8 "tmk_core/common/timer.h"
DEFINED SYMBOLS
                            *ABS*:0000000000000000 eager_pr.c
     /tmp/ccWBsc9f.s:2      *ABS*:000000000000003e __SP_H__
     /tmp/ccWBsc9f.s:3      *ABS*:000000000000003d __SP_L__
     /tmp/ccWBsc9f.s:4      *ABS*:000000000000003f __SREG__
     /tmp/ccWBsc9f.s:5      *ABS*:0000000000000000 __tmp_reg__
     /tmp/ccWBsc9f.s:6      *ABS*:0000000000000001 __zero_reg__
     /tmp/ccWBsc9f.s:13     .text.debounce_init:0000000000000000 debounce_init
     /tmp/ccWBsc9f.s:437    .bss.debounce_counters:0000000000000000 debounce_counters
     /tmp/ccWBsc9f.s:65     .text.update_debounce_counters:0000000000000000 update_debounce_counters
     /tmp/ccWBsc9f.s:432    .bss.counters_need_update:0000000000000000 counters_need_update
     /tmp/ccWBsc9f.s:176    .text.transfer_matrix_values:0000000000000000 transfer_matrix_values
     /tmp/ccWBsc9f.s:442    .bss.matrix_need_update:0000000000000000 matrix_need_update
     /tmp/ccWBsc9f.s:262    .text.debounce:0000000000000000 debounce
     /tmp/ccWBsc9f.s:427    .bss.time.6413:0000000000000000 time.6413
     /tmp/ccWBsc9f.s:422    .bss.last_result.6414:0000000000000000 last_result.6414
     /tmp/ccWBsc9f.s:405    .text.debounce_active:0000000000000000 debounce_active

UNDEFINED SYMBOLS
malloc
timer_read
__udivmodhi4
__do_clear_bss
